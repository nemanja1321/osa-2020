package rs.projekatOSA2020.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account implements Serializable {
	
	private static final long serialVersionUID = 1L;

	//klasa gde definisemo izgled u bazi
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "account_id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "smtpAdress", unique = true, nullable = false, length = 250)
	private String smtpAdress;
	
	@Column(name = "smtpPort", unique = false, nullable =false)
	private Integer smtpPort;
	
	@Column(name = "inServerType", unique = false, nullable = false)
	private Integer inServerType;
	
	@Column(name = "pop3Imap", unique = false, nullable = false, length = 10)
	private String pop3Imap;
	
	@Column(name = "inServerAdress", unique = false, nullable = false, length = 250)
	private String inServerAdress;
	
	@Column(name = "inServerPort", unique = false, nullable = false)
	private String inServerPort;
	
	@Column(name = "userName", unique = true, nullable = true, length = 20)
	private String userName;
	
	@Column(name = "token", unique = true, nullable = true, length = 200)
	private String token;
	
	@Column(name = "password", unique = true, nullable = true, length = 20)
	private String password;
	
	@Column(name = "displayName", unique = true, nullable = false, length = 100)
	private String displayName;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
	private User user;
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "account")
	private Set<Message> message = new HashSet<Message>();
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "account")
	private Set<Folder> folder = new HashSet<Folder>();
	
	

	public Account(Integer id, String smtpAdress, Integer smtpPort, Integer inServerType, String pop3Imap,
			String inServerAdress, String inServerPort, String userName, String token, String password,
			String displayName, User user, Set<Message> message, Set<Folder> folder) {
		super();
		this.id = id;
		this.smtpAdress = smtpAdress;
		this.smtpPort = smtpPort;
		this.inServerType = inServerType;
		this.pop3Imap = pop3Imap;
		this.inServerAdress = inServerAdress;
		this.inServerPort = inServerPort;
		this.userName = userName;
		this.token = token;
		this.password = password;
		this.displayName = displayName;
		this.user = user;
		this.message = message;
		this.folder = folder;
	}
	

	public Account() {
		super();
	}


	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSmtpAdress() {
		return smtpAdress;
	}

	public void setSmtpAdress(String smtpAdress) {
		this.smtpAdress = smtpAdress;
	}

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}

	public Integer getInServerType() {
		return inServerType;
	}

	public void setInServerType(Integer inServerType) {
		this.inServerType = inServerType;
	}

	public String getPop3Imap() {
		return pop3Imap;
	}

	public void setPop3Imap(String pop3Imap) {
		this.pop3Imap = pop3Imap;
	}

	public String getInServerAdress() {
		return inServerAdress;
	}

	public void setInServerAdress(String inServerAdress) {
		this.inServerAdress = inServerAdress;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Set<Message> getMessage() {
		return message;
	}

	public void setMessage(Set<Message> message) {
		this.message = message;
	}

	public Set<Folder> getFolder() {
		return folder;
	}

	public void setFolder(Set<Folder> folder) {
		this.folder = folder;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", smtpAdress=" + smtpAdress + ", smtpPort=" + smtpPort + ", inServerType="
				+ inServerType + ", pop3Imap=" + pop3Imap + ", inServerAdress=" + inServerAdress + ", userName="
				+ userName + ", token=" + token + ", password=" + password + ", displayName=" + displayName
				+ ", message=" + message + ", folder=" + folder + "]";
	}
	
	

}
