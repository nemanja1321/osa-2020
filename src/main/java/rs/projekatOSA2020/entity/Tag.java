package rs.projekatOSA2020.entity;

import java.io.Serializable;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "tag")
public class Tag  implements Serializable{
	//klasa gde definisemo izgled u bazi
	
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tag_id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "tag_name", unique = false, nullable = false)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
	private User user;
	
	@ManyToMany(mappedBy = "tag")
	private Set<Message> message = new HashSet<Message>();
	
	
	public Tag(Integer id, String name, User user, Set<Message> message) {
		super();
		this.id = id;
		this.name = name;
		this.user = user;
		this.message = message;
	}
	
	
	public Tag() {
		super();
	}



	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	public Set<Message> getMessage() {
		return message;
	}


	public void setMessage(Set<Message> message) {
		this.message = message;
	}


	@Override
	public String toString() {
		return "Tag [id=" + id + ", name=" + name + ", user=" + user + ", message=" + message + "]";
	}
	
	

}
