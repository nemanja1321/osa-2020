package rs.projekatOSA2020.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "attachemt")
public class Attachment implements Serializable {
	
	private static final long serialVersionUID = 1L;

	//klasa gde definisemo izgled u bazi
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "attachemnt_id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "data", unique = false, nullable = true)
	private String data;
	
	@Column(name = "mimeType", unique = false, nullable = false, length = 20)
	private String mimeType;
	
	@Column(name = "attachemnt_name", unique = false, nullable = false, length = 100)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "message_id", referencedColumnName = "message_id", nullable = false)
	private Message message;
	
	public Attachment(Integer id, String data, String mimeType, String name, Message message) {
		super();
		this.id = id;
		this.data = data;
		this.mimeType = mimeType;
		this.name = name;
		this.message = message;
	}

	public Attachment() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "Attachment [id=" + id + ", data=" + data + ", mimeType=" + mimeType + ", name=" + name + ", message="
				+ message + "]";
	}
	
	

}
