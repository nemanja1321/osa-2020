$(document).ready(function() {
	
	$('#navKontakti').on('click', function(event) {
		window.location = 'contacts.html';
		
	});
	
	$('#navFolderi').on('click', function(event) {
		window.location = 'folder.html';
		
	});
	
	$('#navNalozi').on('click', function(event) {
		window.location = 'accounts.html';
		
	});
	
	
	
	$('#navSinhronizuj').on('click', function(event) {
		
		get('/messages/sync',  function(data) {
			console.log('radi sink!');
			UcitajPoruke();
		}, function err(e) {
			//console.log(e);
		});
		
		event.preventDefault();
		return false;
	});
	
	var btnClose = document.getElementById('btnClose');
	btnClose.onclick = function() {
		novMailModal.style.display = 'none';
		}
	
	$('#navKreiranje').on('click', function(e){
		novMailModal.style.display = 'block';
		
	});
	$('#navOdjava').on('click', function(e){
		localStorage.removeItem("token");
		window.location = 'index.html';
		
	});
	
	$('#listaMailova').on('click', 'tr', function(){
		var selectedRows = $('#listaMailova').find('tr.selected');
		selectedRows.removeClass('selected');
		
		$('#listaMailova').find('tr').attr('style', 'background-color: none');
		$(this).addClass('selected');
		$(this).attr('style', 'background-color: lightgrey');
	});
	
	
	
	function UcitajPoruke() {
		console.log("ucitavanje...");
		get('/messages/list' , function(data) {
			var table = $('#listaMailova tbody');
			table.empty();
			
			data.forEach(function(message) {
				var unreadClass = message.procitano == true ? "unread" : "";
				table.append(`	<tr>
									<td><a href="#" data-username="${message.id}" class="message-link ${unreadClass}">${message.sender}</td>
									<td>${message.subject}</td>
									<td>` + new Date(Date.parse(message.dateTime)).toLocaleString() + `</td>
								</tr>`);
			});
			//console.log(data);
		}, function(error) {
			console.log(error);
			window.location = "index.html";
		})
	}
	
	UcitajPoruke();
	
	
	$('#btnSend').on('click', function(e) {
		var recipient = $('#recipient').val();
		var cc = $('#cc').val();
		var bcc = $('#bcc').val();
		var subject = $('#subject').val();
		var content = $('#content').val();
		
		$('div.invalid-feedback').css('display', 'none');
		var errorsFound = false;
		
		if(recipient == null || recipient == ""){
			errorsFound = true;
			$('#errorTo').css('display', 'block');}
		
		
		var toSend = {
				"recipient": recipient, "subject":subject, "content":content
		};
		
		toSend.ccs = cc;
		toSend.bccs = bcc;
		console.log(toSend);
		if(errorsFound == false){
			
			post('/messages/send', toSend, function(data) {
				alert("OK");
			}, function(err) {
			alert(err);
		});
			
		}
		
		
		return false;
	});
	
	$('#btnObrisi').on('click', function(event) {
		var odabranaPoruka = $('#listaMailova tbody tr.selected td:first').find('a');
		var idPoruke = odabranaPoruka.attr('data-username');
		var potvrda = confirm('Da li ste sigurni da zelite da obrisete poruku?');
		if(potvrda == true) {
			del('/messages/delete/' + idPoruke, null, function(data) {
				$('#listaMailova tbody tr.selected').remove();
			}, function(e) {
				console.log(e);
			});
			
		}

		event.preventDefault();
		return false;
	});
	
	
	$('body').on('click', 'a.message-link', function(e) {
		var username = $(this).data('username');
		console.log(username);
		get('/messages/' + username, function(data) {
			console.log(data);
			var modal = $('#mailModal');
			modal.show();
			$('#toMail').val(data.recipient);
			$('#subjectMail').val(data.subject);
			$('#contentMail').empty();
			$('#contentMail').append(data.content);
			
			put('/messages/viewed/' + data.id, null, function(dataV) {
				console.log("Procitana poruka!");
			}, function(errorV) {
				console.log(errorV);
			});
		}, function(error) {
			console.log(error);
		})
		e.preventDefault();
		return false;
	});
	
	var btnClose = document.getElementById('btnCloseMail');
	
	btnClose.onclick = function() {
		mailModal.style.display = 'none';
		UcitajPoruke();
		}

});