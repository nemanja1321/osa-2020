$(document).ready(function(){
	
	$('#navKontakti').on('click', function(event) {
		window.location = 'contacts.html';
		
	});
	
	$('#navFolderi').on('click', function(event) {
		window.location = 'folder.html';
		
	});
	
	$('#navMailovi').on('click', function(event) {
		window.location = 'main.html';

	});
	$('#navOdjava').on('click', function(e){
		localStorage.removeItem("token");
		window.location = 'index.html';
		
	});
	
	var btnClose = document.getElementById('btnCloseModal');
	btnClose.onclick = function() {
		nalogModal.style.display = 'none';
		}
	
	$('#tabelaNalozi').on('click', 'tr', function(){
		var selectedRows = $('#tabelaNalozi').find('tr.selected');
		selectedRows.removeClass('selected');
		$('#tabelaNalozi').find('tr').attr('style', 'background-color: none');
		$(this).addClass('selected');
		$(this).attr('style', 'background-color: lightgrey');
	});
	
	$('body').on('click', 'a.account-link', function(e) {		
		var id = $(this).data('id');
		var modal = $('#nalogModal');
		modal.show();
		get('/accounts/' + id, function(data) {
			$('#display').val(data.displayName);
			$('#smtpAddress').val(data.smtpAddress);
			$('#pop3Imap').val(data.pop3Imap);
			$('#smtpPort').val(data.smtpPort);
			$('#inServerType').val(data.inServerType);
			$('#inServerAddress').val(data.inServerAddress);
			$('#inServerPort').val(data.inServerPort);
			$('#username').val(data.username);
			$('#password').val(data.password);
		}, function(error) {
			console.log(error);
		})
		e.preventDefault();
		return false;
	});
	
	function UcitajNaloge() {
		get('/accounts/list' , function(data) {
			var table = $('#tabelaNalozi tbody');
			table.empty();
			//console.log(data);
			
			data.forEach(function(account) {
				table.append(`	<tr>
									<td><a href="#" data-id="${account.id}" class="account-link">${account.username}</td>
									<td>${account.smtpAddress}</td>
									<td>${account.pop3Imap}</td>
								</tr>`);
			});
		}, function(error) {
			console.log(error);
		})
	}
	UcitajNaloge();
	

	
	$('#btnObrisi').on('click', function(event) {
		var id = $(this).data('id');
		var odabranNalog = $('#tabelaNalozi tbody tr.selected td:first').find('a');
		var idNaloga = odabranNalog.attr('data-id');
		var potvrda = confirm('Da li ste sigurni da zelite da obrisete nalog?');
		if(potvrda == true) {
			del('/accounts/delete/' + idNaloga, null, function(data) {
				$('#tabelaNalozi tbody tr.selected').remove();
				if( idNaloga = id)
				  localStorage.removeItem("token");
					window.location = 'index.html';
			}, function(e) {
				console.log(e);
			});
			
		}

		event.preventDefault();
		return false;
	});
	
	
	$('#btnIzmena').on('click', function(event) {
		var selectedRow = $('#tabelaNalozi').find('tr.selected a');
		if(selectedRow == null || selectedRow == undefined) {
			alert("Selektujte red!");
			return false;
		}
		$('div.invalid-feedback').css('display', 'none');
		

		var user = selectedRow.data('id');
		var display = $('#display').val();
		var smtpAddress = $('#smtpAddress').val();
		var pop3Imap =$('#pop3Imap').val();
		var smtpPort = $('#smtpPort').val();
		var inServerType = $('#inServerType').val();
		var inServerAddress = $('#inServerAddress').val();
		var inServerPort =$('#inServerPort').val();
		var username = $('#username').val();
		var password = $('#password').val();
		
		var errorsFound = false;
		
		if(smtpAddress == null || smtpAddress == "") {
			errorsFound = true;
			$('#smtpAddressError').css('display', 'block');
		}
		if(display == null || display == "") {
			errorsFound = true;
			$('#displayError').css('display', 'block');
		}
		if(pop3Imap == null || pop3Imap == "") {
			errorsFound = true;
			$('#pop3ImapError').css('display', 'block');
		}
		if(smtpPort == null || smtpPort == "") {
			errorsFound = true;
			$('#smtpPortError').css('display', 'block');
		}
		if(inServerType == null || inServerType == "") {
			errorsFound = true;
			$('#inServerTypeError').css('display', 'block');
		}
		if(inServerAddress == null || inServerAddress == "") {
			errorsFound = true;
			$('#inServerAddressError').css('display', 'block');
		}
		if(inServerPort == null || inServerPort == "") {
			errorsFound = true;
			$('#inServerPortError').css('display', 'block');
		}
		if(username == null || username == "") {
			errorsFound = true;
			$('#usernameError').css('display', 'block');
		}
		
		
		var pop3Imap =$('#pop3Imap').val();
		var smtpPort = $('#smtpPort').val();
		var inServerType = $('#inServerType').val();
		var inServerAddress = $('#inServerAddress').val();
		var inServerPort =$('#inServerPort').val();
		var username = $('#username').val();
		var password = $('#password').val();

		if(errorsFound == false) {
			put('/accounts/' + user, {
				"id": user,
				"displayName": display,
				"smtpAddress": smtpAddress, 
				"pop3Imap": pop3Imap, 
				"smtpPort": smtpPort, 
				"inServerType": inServerType,
				"inServerAddress": inServerAddress, 
				"inServerPort": inServerPort, 
				"username": username,
				"password": password
			}, function(data){
				alert('izmenjen account!');
				UcitajNaloge();
				$('#nalogModal').hide();
			}, function err(e) {
				console.log(e);
				console.log('greska kod izmene ');
			});	
		}
		
		event.preventDefault();
		return false;
	});
	
	
	
	
	
});