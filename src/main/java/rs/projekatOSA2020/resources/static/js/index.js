	$(document).ready(function() {
				
				localStorage.removeItem("token");

				var btnClose = document.getElementById('btnClose');
				var btnCloseReg = document.getElementById('btnCloseReg');
				
				btnClose.onclick = function() {
					prijavaModal.style.display = 'none';
					}
				btnCloseReg.onclick = function(){
					registracijaModal.style.display = 'none';
				}
				
				$('#navRegistracija').on('click', function(e){
					registracijaModal.style.display = 'block';
					
				});
				$('#btnRegistracija').on('click', function(e) {
					var username = $('#usernameReg').val();
					var password = $('#passReg').val();
					var name = $('#nameReg').val();
					var lastname = $('#lastnameReg').val();
					
					$('div.invalid-feedback').css('display', 'none');
					var errorsFound = false;
					
					if(username == null || username == ""){
						errorsFound = true;
						$('#usernameErrorLogin').css('display', 'block');}
					
					if(password == null || password == ""){
						errorsFound = true;
						$('#lozinkaGreskaPri').css('display', 'block');}
					
					if(name == null || name == ""){
						errorsFound = true;
						$('#nameErrorLogin').css('display', 'block');
					}
					
					if(lastname == null || lastname == ""){
						errorsFound = true;
						$('#lastnameErrorLogin').css('display', 'block');}
					
					if(errorsFound == false){
						
						post('/accounts/registrationUser/' + username + '/' + password + '/' + name + '/' + lastname, null,
								function(data) {
									console.log('Registracija je uspesna!');
									window.location = 'main.html';
									}, function err(e) {
									console.log(e);
									if(e.status == 403) {
										alert("Korisnik vec postoji!");
									}else
										console.log('greska kod registracije');
								});	
						
					}
							
					e.preventDefault();
					return false;
				});
				$('#navPrijava').on('click', function(e){
					prijavaModal.style.display = 'block';
					
				});
				$('#btnPrijava').on('click', function(e) {
					var user = $('#username').val();
					var pass = $('#password').val();
					
					$('div.invalid-feedback').css('display', 'none');
					var errorsFound = false;
					
					if(user == null || user == ""){
						errorsFound = true;
						$('#korImeGreskaPri').css('display', 'block');}
					
					if(pass == null || pass == ""){
						errorsFound = true;
						$('#lozinkaGreskaPri').css('display', 'block');}
					
					if(errorsFound == false){
						
						post('/accounts/login', {
							'username': user,
							'password': pass
						}, function(data) {
							console.log('Login je uspesan!');
							console.log(data);
							var deserialized = JSON.parse(data);
							localStorage.setItem("token", deserialized.access_token);
							window.location = 'main.html';
						}, function err(e) {
							console.log(e);
						});
						
					}
									
					e.preventDefault();
					return false;
				});
			});