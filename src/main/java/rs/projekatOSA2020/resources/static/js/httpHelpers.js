$baseUrl = "https://localhost:8443";

function get(route, ok, err) {
	var token = localStorage.getItem("token");
	console.log(token);
	$.ajax({
        type: "GET",
        method: "GET",
        url: $baseUrl + route,
		headers:{Authorization:"Bearer " + token},
        cache: false,
        success: function (data) {
        	ok(data);
        	console.log($baseUrl + route);
        },
        error:function(xhr, status, text) {
        	if(xhr.responseText != null && xhr.responseText != undefined) {
        		if(xhr.status != 400) {
                    var response = $.parseJSON(xhr.responseText);
                    if(response.status == 401)
                    	window.location = 'index.html';
                    else {
                        console.log(response);

                        if (response) {
                            console.log(response.error);
                        } else {
                            // This would mean an invalid response from the server - maybe the site went down or whatever...
                        }
                    }
        		}
                err(response);
        	} else {
        		err("Nepoznata greska na mrezi!");
        	}
        }
    });	
}


function post(route, parameters, ok, err) {
	var token = localStorage.getItem("token");
	$.ajax({
        type: "POST",
        method: "POST",
        url: $baseUrl + route,
        contentType: "application/json",
		headers:{Authorization:"Bearer " + token},
        dataType: "text",
        cache: false,
        data: JSON.stringify(parameters),
        success: function (data) {
        	ok(data);
        	console.log($baseUrl + route);
        },
        error:function(xhr, status, text) {
            var response = $.parseJSON(xhr.responseText);
            if(response.status == 401)
            	window.location = 'index.html';
            else {
                console.log(response);
                response.status = xhr.status;
                if (response) {
                    console.log(response.error);
                } else {
                    // This would mean an invalid response from the server - maybe the site went down or whatever...
                }
            }
            err(response);
        }
    });	
}

function put(route, parameters, ok, err) {
	var token = localStorage.getItem("token");
	$.ajax({
        type: "PUT",
        method: "PUT",
        url: $baseUrl + route,
        cache: false,
        contentType: "application/json",
		headers:{Authorization:"Bearer " + token},
        data: JSON.stringify(parameters),
        success: function (data) {
        	ok(data);
        	console.log($baseUrl + route);
        },
        error:function(xhr, status, text) {
            var response = $.parseJSON(xhr.responseText);

            if(response.status == 401)
            	window.location = 'index.html';
            else {
                response.status = xhr.status;
                console.log(response);

                if (response) {
                    console.log(response.error);
                } else {
                    // This would mean an invalid response from the server - maybe the site went down or whatever...
                }
            }
            
            err(response);
            
            return false;
        }
    });	
}
function del(route, parameters, ok, err) {
	var token = localStorage.getItem("token");
	$.ajax({
        type: "DELETE",
        method: "DELETE",
        url: $baseUrl + route,
        cache: false,
		headers:{Authorization:"Bearer " + token},
        data: JSON.stringify(parameters),
        success: function (data) {
        	ok(data);
        	console.log($baseUrl + route);
        },
        error:function(xhr, status, text) {

            if(xhr.status == 401)
            	window.location = 'index.html';
            else {
            	if(xhr.status != 403) {

                    var response = $.parseJSON(xhr.responseText);
                    console.log(response);

                    if (response) {
                        console.log(response.error);
                    } else {
                        // This would mean an invalid response from the server - maybe the site went down or whatever...
                    }
                	err(response);
            	} else
                	err("Doslo je do greske!");
            }
        }
    });	
}