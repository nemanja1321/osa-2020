$(document).ready( function() {
	
	
	$('#navMailovi').on('click', function(event) {
		window.location = 'main.html';
		
	});
	
	$('#navKontakti').on('click', function(event) {
		window.location = 'contacts.html';
		
	});
	$('#navFolderi').on('click', function(event) {
		window.location = 'folder.html';
		
	});
	
	$('#navNalozi').on('click', function(event) {
		window.location = 'accounts.html';
		
	});
	$('#navOdjava').on('click', function(event) {
		localStorage.removeItem("token");
		window.location = 'index.html';
	});
	
    var tabelaFolderi = $('#tabelaFolderi');
	
	$('#tabelaFolderi').on('click', 'tr', function(){
		var selectedRows = $('#tabelaFolderi').find('tr.selected');
		selectedRows.removeClass('selected');
		
		$('#tabelaFolderi').find('tr').attr('style', 'background-color: none');
		$(this).addClass('selected');
		$(this).attr('style', 'background-color: lightgrey');
    });

	$('#tabelaPoruke').on('click', 'tr', function(){
		var selectedRows = $('#tabelaPoruke').find('tr.selected');
		selectedRows.removeClass('selected');
		
		$('#tabelaPoruke').find('tr').attr('style', 'background-color: none');
		$(this).addClass('selected');
		$(this).attr('style', 'background-color: lightgrey');
    });
	
	function UcitajPodatke() {
	
			get('/folders/accounts' , function(data) {
				var table = $('#tabelaFolderi tbody');
				table.empty();
				
				data.forEach(function(folder) {
					table.append(`	<tr>
										<td><a href="#" data-id="${folder.id}" class="folder-link">${folder.name}</td>
									</tr>`);
				});
				//console.log(data);
			}, function(error) {
				console.log(error);
	        })
	
	
	}
	UcitajPodatke();

    
    $('#btnObrisiF').on('click', function(event) {
		var folder = $('#tabelaFolderi tbody tr.selected td:first').find('a');
		var idFoldera = folder.attr('data-id');
		var potvrda = confirm('Da li ste sigurni da zelite da obrisete folder?');
		if(potvrda == true) {
			del('/folders/' + idFoldera, null, function(data) {
				$('#tabelaFolderi tbody tr.selected').remove();
			}, function(e) {
				alert('Nije moguce obrisati odabrani folder!');
				console.log(e);
			});

		}
		event.preventDefault();
		return false;
	});
    
	$('body').on('click', 'a.folder-link', function(e) {
		var id = $(this).data('id');
		console.log(id);
		get('/messages/folder/' + id, function(data) {
			var tabela = $('#tabelaPoruke tbody');
			tabela.empty();
			
			data.forEach(function(message) {
				tabela.append(`
						<tr>
							<td data-id="${message.id}">${message.sender}</td>
							<td>` + new Date(Date.parse(message.dateTime)).toLocaleString() + `</td>
							<td>${message.subject}</td>
						</tr>
				`);
			});
			
		}, function(error) {
			console.log(error);
		})
		e.preventDefault();
		return false;
	});
	
	var btnClose = document.getElementById('btnCloseModal');
	
	btnClose.onclick = function() {
		folderModal.style.display = 'none';
		}
	
	var btnCloseNew = document.getElementById('btnCloseModalNew');
	
	btnCloseNew.onclick = function() {
		folderNew.style.display = 'none';
		}
	$('#btnDodajFolder').on('click', function(e){
		
		var modal = $('#folderNew');
		modal.show();
		e.preventDefault();
		return false;
	});
	
	
	$('#btnSacuvaj').on('click', function(e){
		var nameNew = $('#nameNew').val();
		
		$('div.invalid-feedback').css('display', 'none');
		var errorsFound = false;
		
		if(nameNew == null || nameNew == ""){
			errorsFound = true;
			$('#nazivGreskaPri').css('display', 'block');}
		
		if(errorsFound == false){
			post('/folders/addFolder', {
				"name": nameNew},
			function(data){
				alert('Dodat nov folder!');
				folderNew.style.display = 'none';
				UcitajPodatke();
				
			}, function err(e) {
				console.log(e);
				console.log('greska kod dodavanja foldera');
			});	
			
		}
		
		event.preventDefault();
		return false;
		});
	
	
	
	$('#btnObrisiporuku').on('click', function(event) {
		var odabranaPoruka = $('#tabelaPoruke tbody tr.selected').find('td:first');
		var id = odabranaPoruka.data('id');
		var potvrda = confirm('Da li ste sigurni da zelite da obrisete poruku?');
		if(potvrda == true) {
			del('/messages/delete/' + id, null, function(data) {
				$('#tabelaPoruke tbody tr.selected').remove();
			}, function(e) {
				console.log(e);
			});
			
		}

		event.preventDefault();
		return false;
	});


});