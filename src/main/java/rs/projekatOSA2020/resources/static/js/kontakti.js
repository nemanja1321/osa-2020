$(document).ready( function() {
	
	$('#navMailovi').on('click', function(event) {
		window.location = 'main.html';
		
	});
	
	$('#navFolderi').on('click', function(event) {
		window.location = 'folderi.html';
		
	});
	
	$('#navNalozi').on('click', function(event) {
		window.location = 'nalozi.html';
		
	});
	$('#navOdjava').on('click', function(event) {
		localStorage.removeItem("token");
		window.location = 'kontakti.html';
	});
	
	var tabelaKontakti = $('#tabelaKontakti');
	
	$('#tabelaKontakti').on('click', 'tr', function(){
		var selectedRows = $('#tabelaKontakti').find('tr.selected');
		selectedRows.removeClass('selected');
		
		$('#tabelaKontakti').find('tr').attr('style', 'background-color: none');
		$(this).addClass('selected');
		$(this).attr('style', 'background-color: lightgrey');
	});
	
	$('body').on('click', 'a.kontakt-link', function(e) {		
		var username = $(this).data('username');
		console.log(username);
		get('/kontakti/' + username, function(data) {
			console.log(data);
			var modal = $('#kontaktModal');
			modal.show();
			$('#display').val(data.display);
			$('#name').val(data.firstName);
			$('#lastname').val(data.lastName);
			$('#email').val(data.email);
			$('#note').val(data.note);
		}, function(error) {
			console.log(error);
		})
		e.preventDefault();
		return false;
	});
	
	var btnClose = document.getElementById('btnClose');
	
	btnClose.onclick = function() {
		kontaktModal.style.display = 'none';
		}
	
	
	function UcitajPodatke() {
		get('/kontakti', function(data) {
			var table = $('#tabelaKontakti tbody');
			table.empty();
			
			data.forEach(function(kontakt) {
				table.append(`	<tr>
									<td><a href="#" data-username="${kontakt.id}" class="kontakt-link">${kontakt.firstName}</td>
									<td>${kontakt.lastName}</td>
									<td>${kontakt.email}</td>
								</tr>`);
			});
			//console.log(data);
		}, function(error) {
			console.log(error);
		})
	}
	
	UcitajPodatke();
	
	
	$('#btnIzmena').on('click', function(event) {
		var selectedRow = $('#tabelaKontakti').find('tr.selected a');
		if(selectedRow == null || selectedRow == undefined) {
			alert("Selektujte red!");
			return false;
		}
		
		var id = selectedRow.data('username');
		var display = $('#display').val();
		var name = $('#name').val();
		var lastname =$('#lastname').val();
		var email = $('#email').val();
		var note = $('#note').val();
		
		$('div.invalid-feedback').css('display', 'none');
		var errorsFound = false;
		
		if(display == null || display == ""){
			errorsFound = true;
			$('#displayError').css('display', 'block');}
		
		if(name == null || name == ""){
			errorsFound = true;
			$('#firstnameError').css('display', 'block');}
		
		if(lastname == null || lastname == ""){
			errorsFound = true;
			$('#lastnameError').css('display', 'block');}
		
		if(email == null || email == ""){
			errorsFound = true;
			$('#emailError').css('display', 'block');}

		if(errorsFound == false){
			put('/kontakti/' + id, {
				"id": id,
				"display": display,
				"firstName": name, 
				"lastName": lastname, 
				"email": email, 
				"note": note
			}, function(data){
				alert('izmenjen kontakt!');
				UcitajPodatke();
				kontaktModal.style.display = 'none';
			}, function err(e) {
				console.log(e);
				console.log('greska kod izmene kontakta');
			});	
			
		}
		
		
		event.preventDefault();
		return false;
	});
	
	$('#btnObrisi').on('click', function(event) {
		var odabranKorisnik = $('#tabelaKontakti tbody tr.selected td:first').find('a');
		var idKorisnika = odabranKorisnik.attr('data-username');
		var potvrda = confirm('Da li ste sigurni da zelite da obrisete korisnika?');
		if(potvrda == true) {
			del('/kontakti/delete/' + idKorisnika, null, function(data) {
				$('#tabelaKontakti tbody tr.selected').remove();
			}, function(e) {
				console.log(e);
			});
			
		}

		event.preventDefault();
		return false;
	});

	var modalNov = $('#novKontaktModal');
	$('#btnDodajKontakt').on('click', function(e) {
		modalNov.show();
	});
	
	var btnCloseNew = document.getElementById('btnCloseNew');
	btnCloseNew.onclick = function() {
		novKontaktModal.style.display = 'none';
		}
	
	$('#btnSacuvajNoviKontakt').on('click', function(data){
		var displayNew = $('#displayNew').val();
		var nameNew = $('#nameNew').val();
		var lastnameNew =$('#lastnameNew').val();
		var emailNew = $('#emailNew').val();
		var noteNew = $('#noteNew').val();
		
		$('div.invalid-feedback').css('display', 'none');
		var errorsFound = false;
		
		if(displayNew == null || displayNew == ""){
			errorsFound = true;
			$('#displayErrorNew').css('display', 'block');}
		
		if(nameNew == null || nameNew == ""){
			errorsFound = true;
			$('#firstnameErrorNew').css('display', 'block');}
		
		if(lastnameNew == null || lastnameNew == ""){
			errorsFound = true;
			$('#lastnameErrornew').css('display', 'block');}
		
		if(emailNew == null || emailNew == ""){
			errorsFound = true;
			$('#emailErrorNew').css('display', 'block');}
		if(errorsFound == false){
			post('/kontakti', {
				"display": displayNew,
				"firstName": nameNew, 
				"lastName": lastnameNew, 
				"email": emailNew, 
				"note": noteNew},
			function(data){
				alert('Dodat nov kontakt!');
				novKontaktModal.style.display = 'none';

				UcitajPodatke();
				
			}, function err(e) {
				console.log(e);
				console.log('greska kod dodavanja kontkta');
			});	
			
		}
		
		event.preventDefault();
		return false;
		});
		
	});
