package rs.projekatOSA2020.dto;

import java.io.Serializable;

import rs.projekatOSA2020.entity.Rule;
import rs.projekatOSA2020.entity.Rule.Condition;
import rs.projekatOSA2020.entity.Rule.Operation;

public class RuleDTO implements Serializable {
	
	private int id;
	private Condition contition;
	private Operation operation;
	
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	public RuleDTO(int id, Condition contition, Operation operation) {
		super();
		this.id = id;
		this.contition = contition;
		this.operation = operation;
	}

	public RuleDTO(Rule rule) {
		this(rule.getId(), rule.getCondition(), rule.getOperation());
	}

	public RuleDTO() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Condition getContition() {
		return contition;
	}


	public void setContition(Condition contition) {
		this.contition = contition;
	}


	public Operation getOperation() {
		return operation;
	}


	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	
	

}
