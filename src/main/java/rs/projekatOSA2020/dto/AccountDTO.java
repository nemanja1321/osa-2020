package rs.projekatOSA2020.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rs.projekatOSA2020.entity.Account;
import rs.projekatOSA2020.entity.Message;
import rs.projekatOSA2020.entity.User;



public class AccountDTO implements Serializable {
	
	private int id;
	private String smtp;
	private String pop3Imap;
	private String username;
	private String token;
	private String password;
	private List<MessageDTO> message;
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	public AccountDTO(int id, String smtp, String pop3Imap, String username, String token, String password,
			List<MessageDTO> message) {
		super();
		this.id = id;
		this.smtp = smtp;
		this.pop3Imap = pop3Imap;
		this.username = username;
		this.token = token;
		this.password = password;
		this.message = message;
	}
	
	public AccountDTO(Account account) {
		User user = account.getUser();
		List<MessageDTO> messageDTOs = new ArrayList<>();
		if(account.getMessage() != null) {
			for(Message itMessage : account.getMessage()) {
				messageDTOs.add(new MessageDTO(itMessage));
			}
		}
		this.setId(user.getId());
		this.setMessage(messageDTOs);
		this.setPassword(account.getPassword());
		this.setPop3Imap(account.getPop3Imap());
		this.setUsername(account.getUserName());
		this.setToken(account.getToken());
		this.setSmtp(account.getSmtpAdress());
	
				
	}

	public AccountDTO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	public String getPop3Imap() {
		return pop3Imap;
	}

	public void setPop3Imap(String pop3Imap) {
		this.pop3Imap = pop3Imap;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<MessageDTO> getMessage() {
		return message;
	}

	public void setMessage(List<MessageDTO> message) {
		this.message = message;
	}
	
	

}
