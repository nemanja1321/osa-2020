package rs.projekatOSA2020.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import rs.projekatOSA2020.entity.Folder;
import rs.projekatOSA2020.entity.Message;
import rs.projekatOSA2020.entity.Rule;

public class FolderDTO implements Serializable{
	
	private int id;
	private String name;
	private List<FolderDTO> folder;
	private RuleDTO rule;
	private List<MessageDTO> message;
	
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	public FolderDTO(int id, String name, List<FolderDTO> folder, RuleDTO rule, List<MessageDTO> message) {
		super();
		this.id = id;
		this.name = name;
		this.folder = folder;
		this.rule = rule;
		this.message = message;
	}


	public FolderDTO() {
		super();
	}
	
	public FolderDTO (Folder folder) {
		Rule rule = null;
		if(folder.getRule() != null) {
			System.out.println(folder.getRule().size());
			Iterator<Rule> iterator = folder.getRule().iterator();
			while(iterator.hasNext()) {
				System.out.println("While1");
				rule = iterator.next();
			}
		}
		List<FolderDTO> folderDTOs = new ArrayList<>();
		if(folder.getChildren() != null  && !folder.getChildren().isEmpty()) {
			for(Folder folders : folder.getChildren()) {
				List<MessageDTO> messageDTOs = new ArrayList<>();
				for (Message messages : folder.getMessage()) {
					messageDTOs.add(new MessageDTO(messages));
				}
				Rule rules = null;
				Iterator<Rule> iterator = folders.getRule().iterator();
				while(iterator.hasNext()) {
					rules = iterator.next();
				}
				RuleDTO ruleDTO = (rules == null)? null : new RuleDTO(rules);
				FolderDTO folderDTO = new FolderDTO(folders.getId(), folders.getName(), new ArrayList(), ruleDTO, messageDTOs);
				folderDTOs.add(folderDTO);
			}
		}
		List<MessageDTO> messageDTOs = new ArrayList();
		if(folder.getMessage() != null) {
			for( Message message : folder.getMessage()) {
				messageDTOs.add(new MessageDTO(message));
			}
		}
		RuleDTO ruleDTO = (rule == null)? null : new RuleDTO(rule);
		
		this.setId(folder.getId());
		this.setName(folder.getName());
		this.setFolder(folderDTOs);
		this.setRule(ruleDTO);
		this.setMessage(messageDTOs);
	}
	


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<FolderDTO> getFolder() {
		return folder;
	}


	public void setFolder(List<FolderDTO> folder) {
		this.folder = folder;
	}


	public RuleDTO getRule() {
		return rule;
	}


	public void setRule(RuleDTO rule) {
		this.rule = rule;
	}


	public List<MessageDTO> getMessage() {
		return message;
	}


	public void setMessage(List<MessageDTO> message) {
		this.message = message;
	}
	
	

}
