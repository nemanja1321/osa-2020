package rs.projekatOSA2020.dto;

import java.io.Serializable;

import rs.projekatOSA2020.entity.Tag;

public class TagDTO implements Serializable{
	
	private int id;
	private String name;
	
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	public TagDTO(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public TagDTO(Tag tag) {
		this.setId(tag.getId());
		this.setName(tag.getName());
	}


	public TagDTO() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	

}
