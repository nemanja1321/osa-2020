package rs.projekatOSA2020.servise;

import java.util.List;

import rs.projekatOSA2020.entity.Tag;
import rs.projekatOSA2020.entity.User;

public interface TagServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Tag findOne (Integer tagId);
	
	Tag save (Tag tag);
	
	void remove (Integer id);
	
	List<Tag> findAll();
	
	Tag findByNameAndUser (String name, User user);

	

}
