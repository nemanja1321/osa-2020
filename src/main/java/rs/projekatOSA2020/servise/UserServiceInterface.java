package rs.projekatOSA2020.servise;

import java.util.List;

import rs.projekatOSA2020.entity.User;

public interface UserServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	User findOne (Integer userId);
	
	User save (User user);
	
	List<User> findAll();
	
	User findByUsername (String username);
	
	User findByToken (String token);
	
	User findByUsernameAndPassword (String username, String password);

}
