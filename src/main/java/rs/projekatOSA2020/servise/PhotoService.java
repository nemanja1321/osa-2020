package rs.projekatOSA2020.servise;

import org.springframework.beans.factory.annotation.Autowired;

import rs.projekatOSA2020.entity.Photo;
import rs.projekatOSA2020.repository.PhotoRepository;

public class PhotoService implements PhotoServiceIntefrace{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private PhotoRepository photoRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	@Override
	public Photo save(Photo photo) {
		return photoRepository.save(photo);
	}

	@Override
	public Photo findById(int id) {
		return photoRepository.findOne(id);
	}

	@Override
	public void remove(Integer id) {
		photoRepository.delete(id);
		
	}

}
