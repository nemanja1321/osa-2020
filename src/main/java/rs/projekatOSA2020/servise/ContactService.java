package rs.projekatOSA2020.servise;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import rs.projekatOSA2020.entity.Contact;
import rs.projekatOSA2020.repository.ContactRepository;

public class ContactService implements ContactServiceInterface {

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private ContactRepository contactRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	@Override
	public Contact findOne(Integer contactId) {
		return contactRepository.findOne(contactId);
	}

	@Override
	public List<Contact> findAll() {
		return contactRepository.findAll();
	}

	@Override
	public Contact save(Contact contact) {
		return contactRepository.save(contact);
	}

	@Override
	public void remove(Integer id) {
		contactRepository.delete(id);
		
	}

	

}
