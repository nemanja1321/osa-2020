package rs.projekatOSA2020.servise;

import java.util.List;
import java.util.Optional;

import rs.projekatOSA2020.entity.Account;

public interface AccountServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Account save (Account account);
	
	Account findOne(Integer accountId);
	
	List<Account> findAll ();
	
	Account findByUsername (String username);
	
	Account findByToken (String token);
	
	Account findByUsernameAndPassword (String username, String password);

}
