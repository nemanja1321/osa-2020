package rs.projekatOSA2020.servise;

import java.util.List;

import rs.projekatOSA2020.entity.Message;

public interface MessageServiceInteface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Message findOne (Integer messageId);
	
	Message save (Message message);
	
	void remove (Integer id);
	
	List<Message> findAll();
	
	List<Message> findBySubject (String subject);

}
