package rs.projekatOSA2020.servise;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import rs.projekatOSA2020.entity.Account;
import rs.projekatOSA2020.repository.AccountRepository;

public class AccountService implements AccountServiceInterface {

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private AccountRepository accountRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	@Override
	public Account save(Account account) {
		return accountRepository.save(account);
	}
	
	@Override
	public List<Account> findAll() {
		return accountRepository.findAll();
	}

	@Override
	public Account findByUsername(String username) {
		return accountRepository.findByUsername(username);
	}

	@Override
	public Account findByToken(String token) {
		return accountRepository.findByToken(token);
	}

	@Override
	public Account findByUsernameAndPassword(String username, String password) {
		return accountRepository.findByUsernameAndPassword(username, password);
	}

	@Override
	public Account findOne(Integer accountId) {
		return accountRepository.findOne(accountId);
	}
	
	

}
