package rs.projekatOSA2020.servise;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import rs.projekatOSA2020.entity.Tag;
import rs.projekatOSA2020.entity.User;
import rs.projekatOSA2020.repository.TagRepository;

public class TagService implements TagServiceInterface{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private TagRepository tagRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	@Override
	public Tag findOne(Integer tagId) {
		return tagRepository.findOne(tagId);
	}

	@Override
	public Tag save(Tag tag) {
		return tagRepository.save(tag);
	}

	@Override
	public void remove(Integer id) {
		tagRepository.delete(id);
		
	}

	@Override
	public List<Tag> findAll() {
		return tagRepository.findAll();
	}

	@Override
	public Tag findByNameAndUser(String name, User user) {
		return tagRepository.fingByNameAndUser(name, user);
	}
}
