package rs.projekatOSA2020.servise;

import rs.projekatOSA2020.entity.Photo;

public interface PhotoServiceIntefrace {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Photo save (Photo photo);
	
	Photo findById (int id);
	
	void remove (Integer id);

}
