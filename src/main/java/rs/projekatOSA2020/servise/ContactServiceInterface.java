package rs.projekatOSA2020.servise;

import java.util.List;

import rs.projekatOSA2020.entity.Contact;

public interface ContactServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Contact findOne (Integer contactId);
	
	List<Contact> findAll();
	
	Contact save (Contact contact);
	
	void remove (Integer id);

}
