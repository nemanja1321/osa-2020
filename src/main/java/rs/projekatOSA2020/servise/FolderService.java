package rs.projekatOSA2020.servise;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import rs.projekatOSA2020.entity.Account;
import rs.projekatOSA2020.entity.Folder;
import rs.projekatOSA2020.repository.FolderRepository;

public class FolderService implements FolderServiceInterface{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private FolderRepository folderRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	@Override
	public Folder findOne(Integer folderId) {
		return folderRepository.findOne(folderId);
	}

	@Override
	public List<Folder> findAll() {
		return folderRepository.findAll();
	}

	@Override
	public Folder save(Folder folder) {
		return folderRepository.save(folder);
	}

	@Override
	public void delete(Integer id) {
		folderRepository.delete(id);
		
	}

	@Override
	public Folder findByName(String name) {
		return folderRepository.findByName(name);
	}

	@Override
	public Folder findByNameAndAccount(String name, Account account) {
		return folderRepository.findByNameAndACC(name, account);
	}

	@Override
	public List<Folder> findByParent(Folder folder) {
		return folderRepository.findByParent(folder);
	}

}
