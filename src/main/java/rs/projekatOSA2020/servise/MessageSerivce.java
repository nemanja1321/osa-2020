package rs.projekatOSA2020.servise;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import rs.projekatOSA2020.entity.Message;
import rs.projekatOSA2020.repository.MessageRepository;

public class MessageSerivce implements MessageServiceInteface{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private MessageRepository messageRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	@Override
	public Message findOne(Integer messageId) {
		return messageRepository.findOne(messageId);
	}

	@Override
	public Message save(Message message) {
		return messageRepository.save(message);
	}

	@Override
	public void remove(Integer id) {
		messageRepository.delete(id);
		
	}

	@Override
	public List<Message> findAll() {
		return messageRepository.findAll();
	}

	@Override
	public List<Message> findBySubject(String subject) {
		return messageRepository.findBySubject(subject);
	}

}
