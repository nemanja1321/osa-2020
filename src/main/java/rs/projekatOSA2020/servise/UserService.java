package rs.projekatOSA2020.servise;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import rs.projekatOSA2020.entity.User;
import rs.projekatOSA2020.repository.UserRepository;

public class UserService implements UserServiceInterface{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private UserRepository userRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	@Override
	public User findOne(Integer userId) {
		return userRepository.findOne(userId);
	}

	@Override
	public User save(User user) {
		return userRepository.save(user);
	}

	@Override
	public List<User> findAll() {
		return userRepository.findAll();
	}

	@Override
	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	@Override
	public User findByToken(String token) {
		return userRepository.findByToken(token);
	}

	@Override
	public User findByUsernameAndPassword(String username, String password) {
		return userRepository.findByUsernameAndPassword(username, password);
	}

}
