package rs.projekatOSA2020.servise;

import java.util.List;

import rs.projekatOSA2020.entity.Account;
import rs.projekatOSA2020.entity.Folder;

public interface FolderServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Folder findOne (Integer folderId);
	
	List<Folder> findAll();
	
	Folder save (Folder folder);
	
	void delete (Integer id);
	
	Folder findByName (String name);
	
	Folder findByNameAndAccount(String name, Account account);
	
	List<Folder> findByParent (Folder folder);

}
