package rs.projekatOSA2020.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2020.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	
	List<Message> findBySubject (String subject);
}
