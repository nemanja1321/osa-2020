package rs.projekatOSA2020.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2020.entity.Rule;

public interface RuleRepository extends JpaRepository<Rule, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
}
