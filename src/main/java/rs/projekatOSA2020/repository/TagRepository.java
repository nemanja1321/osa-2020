package rs.projekatOSA2020.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2020.entity.Tag;
import rs.projekatOSA2020.entity.User;

public interface TagRepository extends JpaRepository<Tag, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	
	List<Tag> findByUser (User user);
	
	Tag fingByNameAndUser (String name, User user);
}
