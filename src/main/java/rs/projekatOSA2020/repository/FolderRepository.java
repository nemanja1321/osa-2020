package rs.projekatOSA2020.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2020.entity.Account;
import rs.projekatOSA2020.entity.Folder;

public interface FolderRepository extends JpaRepository<Folder, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	List<Folder> findByParent(Folder parent);
	
	Folder findByName (String name);
	
	Folder findByNameAndACC(String name, Account account);

}
