package rs.projekatOSA2020.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2020.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	
	User findByUsername (String username);
	
	User findByToken (String token);
	
	User findByUsernameAndPassword (String username, String password);
}
