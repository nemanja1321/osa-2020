package rs.projekatOSA2020.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2020.entity.Attachment;

public interface AttachmentReposutory extends JpaRepository<Attachment, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
}
