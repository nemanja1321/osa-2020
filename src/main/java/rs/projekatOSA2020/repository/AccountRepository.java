package rs.projekatOSA2020.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.projekatOSA2020.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	Account findByUsername(String username);
	
	Account findByToken(String token);
	
	Account findByUsernameAndPassword(String username, String password);

}
