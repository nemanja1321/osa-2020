package rs.uns.ftn.ProjekatOSA.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "contacts")
public class Contact implements Serializable {
	
	private static final long serialVersionUID = 1L;

	//klasa gde definisemo izgled u bazi
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "contact_id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "firstName", unique = false, nullable = true, length = 100)
	private String firstname;
	
	@Column(name = "lastName", unique = false, nullable = true, length = 100)
	private String lastname;
	
	@Column(name = "displatName", unique = false, nullable = true, length = 100)
	private String displayName;
	
	@Column(name = "email", unique = true, nullable = true, length = 100)
	private String email;
	
	@Column(name = "note", unique = false, nullable = true)
	@Lob
	private String note;
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "contact")
	private Set<Photo> photo = new HashSet<Photo>();
	
	@ManyToOne
	@JoinColumn(name = "user_id", referencedColumnName = "user_id", nullable = false)
	private User user;
	

	public Contact() {
		super();
	}


	public Contact(Integer id, String firstname, String lastname, String displayName, String email, String note,
			 Set<Photo> photo, User user) {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.displayName = displayName;
		this.email = email;
		this.note = note;
		this.photo = photo;
		this.user = user;
	}


	

	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getFirstname() {
		return firstname;
	}


	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getLastname() {
		return lastname;
	}


	public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getDisplayName() {
		return displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}

	public Set<Photo> getPhoto() {
		return photo;
	}


	public void setPhoto(Set<Photo> photo) {
		this.photo = photo;
	}


	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}


	@Override
	public String toString() {
		return "Contact [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", displayName="
				+ displayName + ", email=" + email + ", note=" + note + ", photo=" + photo + "]";
	}
	
	

}
