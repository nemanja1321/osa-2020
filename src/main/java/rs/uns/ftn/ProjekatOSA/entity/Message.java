package rs.uns.ftn.ProjekatOSA.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "message")
public class Message implements Serializable{
	
	private static final long serialVersionUID = 1L;

	//klasa gde definisemo izgled u bazi
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "message_id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "sender", unique = false, nullable = false, length = 100)
	private String from;
	
	@Column(name = "recipient", unique = false, nullable = false)
	private String to;
	
	@Column(name = "cc", unique = false, nullable = true)
	private String cc;
	
	@Column(name = "bcc", unique = false, nullable = true)
	private String bcc;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "dateTime", unique = false, nullable = true)
	private Date dateTime;
	
	@Column(name = "subject", unique = false, nullable = false, length = 250)
	private String subject;
	
	@Column(name = "content", unique = false, nullable = true)
	@Lob
	private String content;
	
	@Column(name = "unread", unique = false, nullable = false)
	private boolean unread;
	
	@ManyToOne
	@JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
	private Account account;
	
	@ManyToOne
	@JoinColumn(name = "folder_id", referencedColumnName = "folder_id", nullable = false)
	private Folder folder;
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "message")
	private Set<Attachment> attachment = new HashSet<Attachment>();
	
	@ManyToMany
	@JoinTable(name = "message_tags", joinColumns = @JoinColumn(name = "message_id"), inverseJoinColumns = @JoinColumn(name = "tag_id"))
	private Set<Tag> tag = new HashSet<Tag>();
	
	public Message(Integer id, String from, String to, String cc, String bcc, Date dateTime, String subject,
			String content, boolean unread, Account account, Folder folder, Set<Attachment> attachment, Set<Tag> tag) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.dateTime = dateTime;
		this.subject = subject;
		this.content = content;
		this.unread = unread;
		this.account = account;
		this.folder = folder;
		this.attachment = attachment;
		this.tag = tag;
	}

	public Message() {
		super();
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", from=" + from + ", to=" + to + ", cc=" + cc + ", bcc=" + bcc + ", dateTime="
				+ dateTime + ", subject=" + subject + ", content=" + content + ", unread=" + unread + ", folder="
				+ folder + "]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public Date getDateTime() {
		return dateTime;
	}

	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public boolean isUnread() {
		return unread;
	}

	public void setUnread(boolean unread) {
		this.unread = unread;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Folder getFolder() {
		return folder;
	}

	public void setFolder(Folder folder) {
		this.folder = folder;
	}

	public Set<Attachment> getAttachment() {
		return attachment;
	}

	public void setAttachment(Set<Attachment> attachment) {
		this.attachment = attachment;
	}

	public Set<Tag> getTag() {
		return tag;
	}

	public void setTag(Set<Tag> tag) {
		this.tag = tag;
	}
	
	
	

}
