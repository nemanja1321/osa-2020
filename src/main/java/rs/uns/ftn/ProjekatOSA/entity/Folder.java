package rs.uns.ftn.ProjekatOSA.entity;

import java.io.ObjectInputStream.GetField;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "folder")
public class Folder implements Serializable{
	
	private static final long serialVersionUID = 1L;

	//klasa gde definisemo izgled u bazi
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "folder_id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "name", unique = false, nullable = false)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
	private Account account;
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "folder")
	private Set<Rule> rule = new HashSet<Rule>();
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "folder")
	private Set<Message> message = new HashSet<Message>();
	
	@ManyToOne
	@JoinColumn(name = "parent_id", referencedColumnName = "folder_id", nullable = true )
	private Folder parent;
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "parent")
	private Set<Folder> children = new HashSet<Folder>();
	
	
	public Folder(Integer id, String name, Account account, Set<Rule> rule, Set<Message> message, Folder perent,
			Set<Folder> children) {
		super();
		this.id = id;
		this.name = name;
		this.account = account;
		this.rule = rule;
		this.message = message;
		this.parent = perent;
		this.children = children;
	}
	
	public void add(Rule rule) {
		if (rule.getFolder() != null) {
			rule.getFolder().getRule().remove(rule);
		}
		rule.setFolder(this);
		getRule().add(rule);
	}
	public void add(Message message) {
		if(message.getFolder() != null) {
			message.getFolder().getMessage().remove(message);
			
		}
		message.setFolder(this);
		getMessage().add(message);
	}
	public void add(Folder folder) {
		if(folder.getParent() != null) {
			folder.getParent().getChildren().remove(folder);
		}
		folder.setParent(this);
		getChildren().add(folder);
	}
	public void remove(Rule rule) {
		rule.setFolder(null);
		getRule().remove(rule);
	}
	public void remove(Message message) {
		message.setFolder(null);
		getMessage().remove(message);
	}
	public void remove (Folder folder) {
		folder.setParent(null);
		getChildren().remove(folder);
	}


	public Folder() {
		super();
	}


	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Account getAccount() {
		return account;
	}


	public void setAccount(Account account) {
		this.account = account;
	}


	public Set<Rule> getRule() {
		return rule;
	}


	public void setRule(Set<Rule> rule) {
		this.rule = rule;
	}


	public Set<Message> getMessage() {
		return message;
	}


	public void setMessage(Set<Message> message) {
		this.message = message;
	}


	public Folder getParent() {
		return parent;
	}


	public void setParent(Folder perent) {
		this.parent = perent;
	}


	public Set<Folder> getChildren() {
		return children;
	}


	public void setChildren(Set<Folder> children) {
		this.children = children;
	}


	@Override
	public String toString() {
		return "Folder [id=" + id + ", name=" + name + ", perent=" + parent + ", children=" + children + "]";
	}
	
	
	

}
