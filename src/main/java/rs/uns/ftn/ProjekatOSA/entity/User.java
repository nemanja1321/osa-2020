package rs.uns.ftn.ProjekatOSA.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;







@Entity
@Table(name = "user")
public class User  implements Serializable{
	
	private static final long serialVersionUID = 1L;

	//klasa gde definisemo izgled u bazi
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id", unique = true, nullable = false)
	private Integer id;
	
	@Column(name = "user_username", unique = true, nullable = false, length = 20)
	private String userName;
	
	@Column(name = "user_password", unique = true, nullable = false, length = 20)
	private String password;
	
	@Column(name = "user_token", unique = true, nullable = true, length = 200)
	private String token;
	
	@Column(name = "user_firstName", unique = false, nullable = false, length = 100)
	private String firstName;
	
	@Column(name = "user_lastName", unique = false, nullable = false, length = 100)
	private String lastName;
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "user")
	private Set<Contact> contact = new HashSet<Contact>();
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "user")
	private Set<Tag> tag = new HashSet<Tag>();
	
	@OneToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY, mappedBy = "user")
	private Set<Account> account = new HashSet<Account>();
	
	public User(Integer id, String userName, String password, String token, String firstName, String lastName,
			Set<Contact> contact, Set<Tag> tag, Set<Account> account) {
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.token = token;
		this.firstName = firstName;
		this.lastName = lastName;
		this.contact = contact;
		this.tag = tag;
		this.account = account;
	}
	
	public void add (Contact contact) {
		if(contact.getUser() != null) {
			contact.getUser().getContact().remove(contact);
		}
		contact.setUser(this);
		getContact().add(contact);
	}
	public void add(Tag tag) {
		if (tag.getUser() != null) {
			tag.getUser().getTag().remove(tag);
		}
		tag.setUser(this);
		getTag().add(tag);
	}
	
	
	public void add (Account account) {
		if (account.getUser() != null) {
			account.getUser().getAccount().remove(account);
		}
		account.setUser(this);
		getAccount().add(account);
	}
	public void remove(Contact contact) {
		contact.setUser(null);
		getContact().remove(contact);
	}
	public void remove (Tag tag) {
		tag.setUser(null);
		getTag().remove(tag);
	}
	public void remove(Account account) {
		account.setUser(null);
		getAccount().remove(account);
	}

	public User() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<Contact> getContact() {
		return contact;
	}

	public void setContact(Set<Contact> contact) {
		this.contact = contact;
	}

	public Set<Tag> getTag() {
		return tag;
	}

	public void setTag(Set<Tag> tag) {
		this.tag = tag;
	}

	public Set<Account> getAccount() {
		return account;
	}

	public void setAccount(Set<Account> account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", password=" + password + ", token=" + token
				+ ", firstName=" + firstName + ", lastName=" + lastName + ", contact=" + contact + ", account="
				+ account + "]";
	}
	
	

}
