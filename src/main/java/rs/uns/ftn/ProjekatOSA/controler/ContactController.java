package rs.uns.ftn.ProjekatOSA.controler;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.uns.ftn.ProjekatOSA.dto.ContactDTO;
import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.entity.Contact;
import rs.uns.ftn.ProjekatOSA.entity.User;
import rs.uns.ftn.ProjekatOSA.servise.AccountServiceInterface;
import rs.uns.ftn.ProjekatOSA.servise.ContactServiceInterface;
import rs.uns.ftn.ProjekatOSA.servise.UserService;

@RestController
@RequestMapping(value = "/contacts")
public class ContactController {

	@Autowired
	ContactServiceInterface contactServiceInterface;
	@Autowired
	AccountServiceInterface accountServiceInterface;
	@Autowired
	UserService userService;

	@GetMapping
	public ResponseEntity<List<ContactDTO>> getAllContacts(Principal principale) {
		String username = principale.getName();
		User userfromDB = userService.findByUsername(username);

		List<Contact> contacts = contactServiceInterface.findByUserId(userfromDB.getId());
		if (contacts == null) {
			return new ResponseEntity<List<ContactDTO>>(HttpStatus.NOT_FOUND);
		}
		List<ContactDTO> contactsDTO = new ArrayList<ContactDTO>();
		for (Contact contact : contacts) {
			contactsDTO.add(new ContactDTO(contact));
		}
		return new ResponseEntity<List<ContactDTO>>(contactsDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/account/{username}")
	public ResponseEntity<List<ContactDTO>> getAllContactsFromUser(@PathVariable("username") String username,
			Principal user) {
		Account account = accountServiceInterface.findByUsername(username);
		String user1 = user.getName();
		if (account == null) {
			return new ResponseEntity<List<ContactDTO>>(HttpStatus.NOT_FOUND);
		}
		List<ContactDTO> contactDTO = new ArrayList<ContactDTO>();
		if (account.getUser() != null && account.getUser().getContact() != null) {
			for (Contact contact : account.getUser().getContact()) {
				contactDTO.add(new ContactDTO(contact));
			}
		}
		return new ResponseEntity<List<ContactDTO>>(contactDTO, HttpStatus.OK);
	}

	@GetMapping(value = "/emails/account/{username}")
	public ResponseEntity<List<String>> getAllEmails(@PathVariable("username") String username) {
		Account account = accountServiceInterface.findByUsername(username);
		if (account == null) {
			return new ResponseEntity<List<String>>(HttpStatus.BAD_REQUEST);
		}
		List<String> email = new ArrayList<String>();
		if (account.getUser() != null && account.getUser().getContact() != null) {
			for (Contact contact : account.getUser().getContact()) {
				email.add(contact.getEmail());
			}
		}
		return new ResponseEntity<List<String>>(email, HttpStatus.OK);
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<ContactDTO> getContact(@PathVariable("id") Integer id) {
		Optional<Contact> contactOpt = contactServiceInterface.findOne(id);
		if (contactOpt.isPresent()) {
			Contact contact = contactOpt.get();
			return new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.OK);
		}
		return new ResponseEntity<ContactDTO>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Void> deleteContact(@PathVariable("id") Integer id) {
		Optional<Contact> contOptional = contactServiceInterface.findOne(id);
		
		if (contOptional.isPresent()) {
			Contact contact = contOptional.get();
			contactServiceInterface.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping(consumes = "applicaton/json")
	public ResponseEntity<ContactDTO> saveContact(@RequestBody ContactDTO contactDTO, Principal principale) {
		Contact contact = new Contact();
		contact.setId(0);
		contact.setDisplayName(contactDTO.getDisplay());
		contact.setEmail(contactDTO.getEmail());
		contact.setFirstname(contactDTO.getFirstName());
		contact.setLastname(contactDTO.getLastName());
		contact.setNote(contactDTO.getNote());

		String username = principale.getName();
		User userFromDB = userService.findByUsername(username);

		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		Account account1 = account.get(0);
		if (account1 == null) {
			return new ResponseEntity<ContactDTO>(HttpStatus.BAD_REQUEST);
		}
		contact.setUser(account1.getUser());
		contact = contactServiceInterface.save(contact);
		return new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.OK);
	}

	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<ContactDTO> editContact(@RequestBody ContactDTO contactDTO, @PathVariable("id") Integer id,
			Principal principale) {
		String username = principale.getName();
		User userFromDB = userService.findByUsername(username);
		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		Account account1 = account.get(0);
		Optional<Contact> contOptional = contactServiceInterface.findOne(id);
		Contact contact = contOptional.get();
		if (account1 == null || contact == null) {
			return new ResponseEntity<ContactDTO>(HttpStatus.BAD_REQUEST);
		}
		contact.setId(contactDTO.getId());
		contact.setDisplayName(contactDTO.getDisplay());
		contact.setEmail(contactDTO.getEmail());
		contact.setFirstname(contactDTO.getFirstName());
		contact.setLastname(contactDTO.getLastName());
		contact.setNote(contactDTO.getNote());

		account1.getUser().add(contact);
		contactServiceInterface.save(contact);
		return new ResponseEntity<ContactDTO>(new ContactDTO(contact), HttpStatus.OK);
	}

}
