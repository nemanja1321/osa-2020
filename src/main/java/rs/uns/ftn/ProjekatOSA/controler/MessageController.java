package rs.uns.ftn.ProjekatOSA.controler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.channels.SeekableByteChannel;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.StreamingHttpOutputMessage.Body;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.uns.ftn.ProjekatOSA.dto.MessageDTO;
import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.entity.Folder;
import rs.uns.ftn.ProjekatOSA.entity.Message;
import rs.uns.ftn.ProjekatOSA.entity.User;
import rs.uns.ftn.ProjekatOSA.servise.AccountServiceInterface;
import rs.uns.ftn.ProjekatOSA.servise.FolderServiceInterface;
import rs.uns.ftn.ProjekatOSA.servise.MessageServiceInteface;
import rs.uns.ftn.ProjekatOSA.servise.UserServiceInterface;

import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Flags.Flag;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.management.RuntimeErrorException;


@RestController
@RequestMapping(value = "/message")
public class MessageController {
	
	@Autowired
	MessageServiceInteface messageServiceInterface;
	@Autowired
	AccountServiceInterface accountServiceInterface;
	@Autowired
	FolderServiceInterface folderServiceInterface;
	@Autowired
	UserServiceInterface userServiceInterface;
	
	@GetMapping("/list")
	public ResponseEntity<List<MessageDTO>> getAllMessagesFromUser(Principal principal){
		String username = principal.getName();
		User userFromDB = userServiceInterface.findByUsername(username);
		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		if(account == null || account.size() < 1) {
			return new ResponseEntity<List<MessageDTO>>(HttpStatus.BAD_REQUEST);
		}
		Account firstAcc = account.get(0);
		List<MessageDTO> messageDTO = new ArrayList<MessageDTO>();
		for(Message message : firstAcc.getMessage()) {
			messageDTO.add(new MessageDTO(message));
		}
		return new ResponseEntity<List<MessageDTO>>(messageDTO, HttpStatus.OK);
	}
	
	public ResponseEntity<List<MessageDTO>> syncMessage(Principal principal){
		String username = principal.getName();
		User userFromDB = userServiceInterface.findByUsername(username);
		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		if(account == null || account.size() < 1) {
			return new ResponseEntity<List<MessageDTO>>(HttpStatus.BAD_REQUEST);
		}
		Account account1 = account.get(0);
		Folder folder = folderServiceInterface.findByNameAndAccount("Inbox", account1);
		try {
			Properties properties = new Properties();
			properties.put("mail.store.protocol", "pop3");
			properties.put("mail.pop3.host", account1.getInServerAdress());
			properties.put("mail.pop3.port", account1.getInServerPort());
			properties.put("mail.pop3.ssl.trust", "*");
			properties.put("mail.pop3.starttls.enable", "true");
			properties.put("mail.debug", "true");
			
			Session emailSession = Session.getDefaultInstance(properties);
			Store store = emailSession.getStore("pop3s");
			store.connect(account1.getInServerAdress(), account1.getUserName(), account1.getPassword());
			
			javax.mail.Folder emailFolder = store.getFolder("Inbox");
			emailFolder.open(javax.mail.Folder.READ_ONLY);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			javax.mail.Message[] message = emailFolder.getMessages();
			for(int i = 0; i < message.length; i++) {
				javax.mail.Message message1 = message[i];
				Date mailDate = message1.getSentDate() == null ? new Date() : message1.getSentDate();
				String sender = message1.getFrom()[0].toString();
				Optional<Message> fromDB = messageServiceInterface.findByDate(mailDate);
				Message messageFromDB;
				if(fromDB.isPresent()) {
					continue;
				}
				else {
					messageFromDB = new Message();
				}
				messageFromDB.setFrom(sender);
				messageFromDB.setTo(account1.getUserName());
				messageFromDB.setDateTime(mailDate);
				messageFromDB.setSubject(message1.getSubject());
				String cc = "";
				for(int x = 0; x < message1.getAllRecipients().length; x++) {
					if(messageFromDB.getCc() != null) {
						messageFromDB.setCc(messageFromDB.getCc() + ";" + message1.getAllRecipients()[x]);
					}
					else {
						messageFromDB.setCc(message1.getAllRecipients()[x].toString());
					}
					
				}
				String mailContent = "";
				Multipart mp = (Multipart) message1.getContent();
				for(int z = 0; z < mp.getCount(); z++) {
					BodyPart bp = mp.getBodyPart(z);
					String disp = bp.getDescription();
					if(disp != null && (disp.equals(BodyPart.ATTACHMENT))) {
						
					}
					else {
						mailContent += bp.getContent();
					}
				}
				messageFromDB.setContent(mailContent);
				messageFromDB.setUnread(true);
				messageFromDB.setAccount(account1);
				messageFromDB.setFolder(folder);
				messageServiceInterface.save(messageFromDB);
				
					
				
			}
			emailFolder.close(false);
			store.close();
		}
		catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		catch (MessagingException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return new ResponseEntity<List<MessageDTO>>((List<MessageDTO>) null, HttpStatus.OK);
	
	}
	@GetMapping(value = "/folder/{id}")
	public ResponseEntity<List<MessageDTO>> getAllMessageFromFolderUser(@PathVariable("id") Integer id, Principal principal){
		String username = principal.getName();
		User userFromDB = userServiceInterface.findByUsername(username);
		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		Account account1 = account.get(0);
		Optional<Folder> foldOptional = folderServiceInterface.findOne(id);
		Folder folder = foldOptional.get();
		if(account1 == null) {
			return new ResponseEntity<List<MessageDTO>>(HttpStatus.BAD_REQUEST);
		}
		List<MessageDTO> messageDTO = new ArrayList<MessageDTO>();
		for(Message message : account1.getMessage()) {
			if(message.getFolder().getId().equals(folder.getId())) {
				messageDTO.add(new MessageDTO(message));
			}
		}
		return new ResponseEntity<List<MessageDTO>>(messageDTO, HttpStatus.OK); 
	}
	@GetMapping(value = "/{id}")
	public ResponseEntity<MessageDTO> getMessage (@PathVariable("id") Integer id){
		Optional<Message> emailOptional = messageServiceInterface.findOne(id);
		Message message = emailOptional.get();
		if (message == null) {
			return new ResponseEntity<MessageDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Void> deleteMessage(@PathVariable("id") Integer id, Principal pricipal){
		
		String username = pricipal.getName();
		User userFromDB = userServiceInterface.findByUsername(username);
		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		if(account == null || account.size() < 1) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		Account account1 = account.get(0);
		Optional<Message> toDelete = messageServiceInterface.findOne(id);
		if(!toDelete.isPresent()) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
			
		}
		Folder folder = folderServiceInterface.findByNameAndAccount("Inbox", account1);
		try {
			Properties properties = new Properties();
			properties.put("mail.store.protocol", "pop3");
			properties.put("mail.pop3.host", account1.getInServerAdress());
			properties.put("mail.pop3.port", account1.getInServerPort());
			properties.put("mail.pop3.ssl.trust", "*");
			properties.put("mail.pop3.starttls.enable", "true");
			properties.put("mail.debug", "true");
			
			Session emailSession = Session.getDefaultInstance(properties);
			Store store = emailSession.getStore("pop3s");
			
			store.connect(account1.getInServerAdress(), account1.getUserName(), account1.getPassword());
			javax.mail.Folder emailFolder = store.getFolder("Inbox");
			emailFolder.open(javax.mail.Folder.READ_WRITE);
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			
			javax.mail.Message[] message = emailFolder.getMessages();
			for(int i = 0; i < message.length; i++) {
				javax.mail.Message message1 = message[i];
				Date maildate = message1.getSentDate() == null ? new Date() : message1.getSentDate();
				if(maildate == toDelete.get().getDateTime()) {
					message1.setFlag(Flag.DELETED, true);
				}
			}
			emailFolder.close(false);
			store.close();
		}
			
		catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		catch (MessagingException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		Optional<Message> emailOptional = messageServiceInterface.findOne(id);
		Message message = emailOptional.get();
		if(message != null) {
			messageServiceInterface.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(value = "/send", consumes = "application/json")
	public ResponseEntity<MessageDTO> send (@RequestBody MessageDTO messageDTO, Principal principal){
		String username = principal.getName();
		User userFromDB = userServiceInterface.findByUsername(username);
		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		if(account == null || account.size() < 1) {
			return new ResponseEntity<MessageDTO>(HttpStatus.BAD_REQUEST);
		}
		Account account1 = account.get(0);
		Folder folder = folderServiceInterface.findByNameAndAccount("Send", account1);
		String cc = "";
		String bcc = "";
		cc = String.join(";", messageDTO.getCc());
		bcc = String.join(";", messageDTO.getBcc());
		
		Message message = new Message();
		account1.add(message);
		
		message.setDateTime(new Date());
		folder.add(message);
		message.setFrom(account1.getUserName());
		message.setTo(messageDTO.getTo());
		message.setSubject(messageDTO.getSubject());
		message.setContent(messageDTO.getContetn());
		message.setCc(cc);
		message.setBcc(bcc);
		message.setUnread(messageDTO.isRead());
		
		String to  = message.getTo();
		String from  = account1.getUserName();
		final String user = account1.getUserName();
		final String password = account1.getPassword();
		String host = account1.getSmtpAdress();
		Properties properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", account1.getSmtpPort());
		
		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		});
		
		try {
			MimeMessage mime = new MimeMessage(session);
			mime.setFrom(new InternetAddress(from));
			mime.setRecipients(RecipientType.CC, message.getCc());
			mime.setRecipients(RecipientType.BCC, message.getBcc());
			mime.setRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(to));
			mime.setSubject(message.getSubject());
			mime.setText(message.getContent());
			Transport.send(mime);
			messageServiceInterface.save(message);
		}
		catch (MessagingException e) {
			throw new RuntimeException(e);
		}
		return new ResponseEntity<MessageDTO>(new MessageDTO(message), HttpStatus.OK);
		
	}
	
	@PutMapping(value = "/viewwd/{id}")
	ResponseEntity<Void> getViewMessages(@PathVariable("id") Integer id){
		Optional<Message> emailOptional = messageServiceInterface.findOne(id);
		Message message = emailOptional.get();
		if(message == null) {
			return new ResponseEntity<Void>(HttpStatus.BAD_REQUEST);
		}
		message.setUnread(false);
		messageServiceInterface.save(message);
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
}
