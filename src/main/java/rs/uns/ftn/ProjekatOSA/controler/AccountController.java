package rs.uns.ftn.ProjekatOSA.controler;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.naming.AuthenticationException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.uns.ftn.ProjekatOSA.authentication.AuthenticationRequest;
import rs.uns.ftn.ProjekatOSA.authentication.TokenHelper;
import rs.uns.ftn.ProjekatOSA.dto.AccountDTO;
import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.entity.Folder;
import rs.uns.ftn.ProjekatOSA.entity.User;
import rs.uns.ftn.ProjekatOSA.entity.UserTokenState;
import rs.uns.ftn.ProjekatOSA.servise.AccountServiceInterface;
import rs.uns.ftn.ProjekatOSA.servise.FolderServiceInterface;
import rs.uns.ftn.ProjekatOSA.servise.UserServiceInterface;

@RestController
@RequestMapping(value = "/accounts")
public class AccountController {
	
	@Autowired
	private AccountServiceInterface accountServiceinterface;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	TokenHelper tokenHelkper;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	UserServiceInterface userServiceInterface;
	
	@Autowired
	private FolderServiceInterface folderServiceInterface;
	
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseEntity<?> createAutheticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws AuthenticationException{
		final Authentication authentication  = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
																														authenticationRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		UserDetails user = (UserDetails)authentication.getPrincipal();
		String jws = tokenHelkper.generateToken(user.getUsername());
		return ResponseEntity.ok(new UserTokenState(jws));
		
	}
	
	@PostMapping(value = "/registrationUser/{username}/{password}/{firstname}/{lastname}")
	public ResponseEntity<AccountDTO> registrationUser (@PathVariable("username") String username, 
														@PathVariable("password") String password,
														@PathVariable("firstname") String firstname,
														@PathVariable ("lastname") String lastname){
		User tmpUser = userServiceInterface.findByUsername(username);
		if(tmpUser != null) {
			return new ResponseEntity<AccountDTO>(new AccountDTO(), HttpStatus.FORBIDDEN);
		}
		User user = new User();
		user.setFirstName(firstname);
		user.setLastName(lastname);
		user.setUserName(username);
		user.setPassword(password);
		
		userServiceInterface.save(user);
		return new ResponseEntity<AccountDTO>(HttpStatus.OK);
		
	}
	
	@PutMapping(value = "/addAccount/{username}")
	public ResponseEntity<Void> addAccount(@RequestBody AccountDTO accountDTO, @PathVariable("username") String username){
		User user = userServiceInterface.findByUsername(username);
		
		Account account = new Account();
		account.setDisplayName(user.getFirstName() + " " + user.getLastName());
		account.setInServerAdress("ddd" + accountDTO.getUsername());
		account.setInServerPort(2230);
		account.setInServerType(123);
		account.setPassword(accountDTO.getPassword());
		account.setPop3Imap("pop3");
		account.setSmtpAdress("admin" + accountDTO.getUsername());
		account.setSmtpPort(2233);
		account.setUser(user);
		account.setUserName(accountDTO.getUsername());
		
		account = accountServiceinterface.save(account);
		
		Folder folder1 = new Folder();
		folder1.setName("Inbox");
		folder1.setAccount(account);
		Folder folder2 = new Folder();
		folder2.setAccount(account);
		folder2.setName("Sent");
		folder1 = folderServiceInterface.save(folder1);
		folder2 = folderServiceInterface.save(folder2);
		
		account.add(folder1);
		account.add(folder2);
		
		return new ResponseEntity<Void>(HttpStatus.OK);
	}
	
	@GetMapping(value = "/list")
	public ResponseEntity<List<AccountDTO>> getAllAccount(Principal us){
		String username = us.getName();
		List<Account> accounts = accountServiceinterface.findAll();
		
		if(accounts == null) {
			return new ResponseEntity<List<AccountDTO>>(HttpStatus.NOT_FOUND);
		}
		User user = userServiceInterface.findByUsername(username);
		List<AccountDTO> accountDTO = new ArrayList<AccountDTO>();
		for(Account account : accounts) {
			if(user.getId() == account.getUser().getId()) {
				accountDTO.add(new AccountDTO(account));
			}
		}
		return new ResponseEntity<List<AccountDTO>>(accountDTO, HttpStatus.OK);
	}
	@GetMapping(value = "/{id}")
	public ResponseEntity<AccountDTO> getAccount (@PathVariable("id") Integer id){
		Optional<Account> accountOptional = accountServiceinterface.findOne(id);
		if(accountOptional.isPresent()) {
			Account account = accountOptional.get();
			return new ResponseEntity<AccountDTO>(new AccountDTO(account), HttpStatus.OK);
		}
		return new ResponseEntity<AccountDTO>(HttpStatus.NOT_FOUND);
		
	}
	@GetMapping(value = "/username/{username}")
	public ResponseEntity<AccountDTO> getAccountsByUsername(@PathVariable("username") String username){
		Account account = accountServiceinterface.findByUsername(username);
		if(account == null) {
			return new ResponseEntity<AccountDTO>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<AccountDTO>(new AccountDTO(account), HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<AccountDTO> updateAccount (@RequestBody AccountDTO accountDTO, @PathVariable("id") Integer id){
		Optional <Account> accOptional = accountServiceinterface.findOne(id);
		Account account = accOptional.get();
		if(account == null) {
			return new ResponseEntity<AccountDTO>(HttpStatus.NOT_FOUND);
		}
		account.setDisplayName(accountDTO.getDisplay());
		account.setUserName(accountDTO.getUsername());
		account.setPassword(accountDTO.getPassword());
		account.setPop3Imap(accountDTO.getPop3Imap());
		account.setSmtpAdress(accountDTO.getSmtp());
		
		String newPass = accountDTO.getPassword();
		if(newPass != null && newPass != "") {
			account.setPassword(newPass);
		}
		account = accountServiceinterface.save(account);
		return new ResponseEntity<AccountDTO>(new AccountDTO(account), HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/delete/{id}")
	public ResponseEntity<Void> deleteAccount(@PathVariable("id") Integer id){
		Optional<Account> accOptional = accountServiceinterface.findOne(id);
		
		if(accOptional.isPresent()) {
			Account account = accOptional.get();
			accountServiceinterface.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
		}
		else {
			return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
		}
	}
	
}
	

	
