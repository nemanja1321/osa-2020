package rs.uns.ftn.ProjekatOSA.controler;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;



import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.uns.ftn.ProjekatOSA.dto.FolderDTO;
import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.entity.Folder;
import rs.uns.ftn.ProjekatOSA.entity.Message;
import rs.uns.ftn.ProjekatOSA.entity.Rule;
import rs.uns.ftn.ProjekatOSA.entity.User;
import rs.uns.ftn.ProjekatOSA.servise.AccountServiceInterface;
import rs.uns.ftn.ProjekatOSA.servise.FolderServiceInterface;
import rs.uns.ftn.ProjekatOSA.servise.MessageServiceInteface;
import rs.uns.ftn.ProjekatOSA.servise.UserServiceInterface;

@RestController
@RequestMapping(value = "/folders")
public class FolderControler {
	
	@Autowired
	FolderServiceInterface folderServiceInterface;
	@Autowired 
	AccountServiceInterface accountServiceInterface;
	@Autowired
	MessageServiceInteface messageServiceInterface;
	@Autowired
	UserServiceInterface userServiceInteface;
	
	@GetMapping
	public ResponseEntity<List<FolderDTO>> getAllFolders(){
		List<Folder> folders = folderServiceInterface.findAll();
		if(folders == null) {
			return new ResponseEntity<List<FolderDTO>>(HttpStatus.NOT_FOUND);
			
		}
		List<FolderDTO> folderDTO = new ArrayList<FolderDTO>();
		for(Folder folder : folders) {
			folderDTO.add(new FolderDTO(folder));
		}
		return new ResponseEntity<List<FolderDTO>>(folderDTO, HttpStatus.OK);
	}
	@GetMapping(value = "/accounts")
	public ResponseEntity<List<FolderDTO>> getAllFoldersFromUser(Principal princpale){
		String username = princpale.getName();
		User userFromDB = userServiceInteface.findByUsername(username);
		
		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		if(account == null || account.size() < 1) {
			return new ResponseEntity<List<FolderDTO>>(HttpStatus.BAD_REQUEST);
		}
		Account firstAcc = account.get(0);
		List<FolderDTO> folderDTOs = new ArrayList<FolderDTO>();
		for(Folder folder : firstAcc.getFolder()) {
			folderDTOs.add(new FolderDTO(folder));
		}
		return new ResponseEntity<List<FolderDTO>>(folderDTOs, HttpStatus.OK);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<FolderDTO> getFolder(@PathVariable("id") Integer id){
		Optional<Folder> folderOpt = folderServiceInterface.findOne(id);
		
		if(folderOpt.isPresent()) {
			Folder folder = folderOpt.get();
			return new ResponseEntity<FolderDTO>(new FolderDTO(folder), HttpStatus.OK);
		}
		return new ResponseEntity<FolderDTO>(HttpStatus.NOT_FOUND);
	}
	
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Void> deleteFolder(@PathVariable("id") Integer id){
		Optional<Folder> foldOptional = folderServiceInterface.findOne(id);
		
		if(foldOptional.isPresent() && !foldOptional.get().getName().equals("Inbox") && !foldOptional.get().getName().equals("Sent")) {
			Folder folder = foldOptional.get();
			List<Message> messageFromFolder = (List<Message>) folder.getMessage();
			for (Message message : messageFromFolder) {
				if(message == null || message.getId() != null) {
					messageServiceInterface.remove(message.getId());
				}
				
			}
			folderServiceInterface.remove(id);
			return new ResponseEntity<Void>(HttpStatus.OK);
			
		}
		else {
			return new ResponseEntity<Void>(HttpStatus.FORBIDDEN);
		}
		
	}
	
	@PostMapping(value = "/addFolder", consumes = "application/json")
	public ResponseEntity<FolderDTO> addNewFolder(@RequestBody FolderDTO folderDTO, Principal principal){
		String username = principal.getName();
		User userFromDB = userServiceInteface.findByUsername(username);
		List<Account> account = accountServiceInterface.findByUserId(userFromDB.getId());
		Account account1 = account.get(0);
		if(account1 == null) {
			return new ResponseEntity<FolderDTO>(HttpStatus.BAD_REQUEST);
		}
		Folder folder = new Folder();
		folder.setName(folderDTO.getName());
		folder.setRule(null);
		account1.add(folder);
		folder = folderServiceInterface.save(folder);
		return new ResponseEntity<FolderDTO>(new FolderDTO(folder), HttpStatus.OK);
	}
	
	@PutMapping(value = "/{id}", consumes = "application/json")
	public ResponseEntity<FolderDTO> updateFolder(@RequestBody FolderDTO folderDTO, @PathVariable("id") Integer id){
		Optional<Folder> foldOptional = folderServiceInterface.findOne(id);
		Folder folder = foldOptional.get();
		if(folder == null) {
			return new ResponseEntity<FolderDTO>(HttpStatus.BAD_REQUEST);
		}
		folder.setName(folderDTO.getName());
		Rule rule;
		if(folder.getRule().iterator().next() != null) {
			rule = folder.getRule().iterator().next();
		}
		else {
			rule = new Rule();
		}
		rule.setOperation(folderDTO.getRule().getOperation());
		rule.setCondition(folderDTO.getRule().getContition());
		folder.add(rule);
		folder = folderServiceInterface.save(folder);
		return new ResponseEntity<FolderDTO>(new FolderDTO(folder), HttpStatus.OK);
	}
	
}
