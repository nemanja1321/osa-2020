package rs.uns.ftn.ProjekatOSA;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import rs.uns.ftn.ProjekatOSA.authentication.RESTAuthenticationEntryPoint;
import rs.uns.ftn.ProjekatOSA.authentication.TokenAuthFilter;
import rs.uns.ftn.ProjekatOSA.authentication.TokenHelper;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecutiriConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	TokenHelper tokenHelper;
	@Autowired
	UserDetailsService userDetailsService;
	@Autowired
	private RESTAuthenticationEntryPoint restAuthenticationEntryPoint;
	@Bean
	public PasswordEncoder passsEncoder() {
		return new Pbkdf2PasswordEncoder(tokenHelper.SECRET, 1000, 20);
	}
	@Bean
	@Override
	public AuthenticationManager authenticationManager() throws Exception{
		return super.authenticationManagerBean();
	}
	@Override
	public void configure(WebSecurity web) throws Exception{
		web.ignoring().antMatchers(HttpMethod.POST,
                "/accounts/login",
                "/accounts/registrationUser/**");
		web.ignoring().antMatchers(HttpMethod.GET,
                "/",
                "/webjars/**",
                "/bootstrap/**",
                "/*.html",
                "/favicon.ico",
                "/js/**",
                "/css/**");
	}
	@Override
	protected void configure (HttpSecurity http) throws Exception{
		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
		.and().exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint)
		.and().authorizeRequests().antMatchers("/**").permitAll()
		.antMatchers("/accounts/registrationUser/**").permitAll().anyRequest().authenticated()
		.and().addFilterBefore(new TokenAuthFilter(tokenHelper, userDetailsService), BasicAuthenticationFilter.class);
		
		http.csrf().disable();
	}
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
		auth.userDetailsService(userDetailsService).passwordEncoder(passsEncoder());
	}

}
