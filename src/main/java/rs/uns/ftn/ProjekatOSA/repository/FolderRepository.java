package rs.uns.ftn.ProjekatOSA.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.entity.Folder;

public interface FolderRepository extends JpaRepository<Folder, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	List<Folder> findByParent(Folder parent);
	
	Folder findByName (String name);
	
	Folder findByNameAndAccount(String name, Account account);

	

}
