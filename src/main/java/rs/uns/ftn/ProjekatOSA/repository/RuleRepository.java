package rs.uns.ftn.ProjekatOSA.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.uns.ftn.ProjekatOSA.entity.Rule;

public interface RuleRepository extends JpaRepository<Rule, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
}
