package rs.uns.ftn.ProjekatOSA.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.uns.ftn.ProjekatOSA.entity.Photo;

public interface PhotoRepository extends JpaRepository<Photo, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
}
