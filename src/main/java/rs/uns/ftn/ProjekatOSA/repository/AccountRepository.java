package rs.uns.ftn.ProjekatOSA.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.uns.ftn.ProjekatOSA.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	Account findByUserName(String username);
	
	Account findByToken(String token);
	
	Account findByUserNameAndPassword(String username, String password);

	Optional<Account> findById(Integer accountId);

	List<Account> findByUserId(Integer id);

}
