package rs.uns.ftn.ProjekatOSA.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.uns.ftn.ProjekatOSA.entity.User;

public interface UserRepository extends JpaRepository<User, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	
	User findByUserName (String username);
	
	User findByToken (String token);
	
	User findByUserNameAndPassword (String username, String password);
}
