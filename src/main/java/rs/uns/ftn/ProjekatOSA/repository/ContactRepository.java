package rs.uns.ftn.ProjekatOSA.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.uns.ftn.ProjekatOSA.entity.Contact;

public interface ContactRepository extends JpaRepository<Contact, Integer> {
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	Contact findByEmail(String email);

	List<Contact> findByUserId(Integer id);

	Optional<Contact> findById(Integer contactId);

}
