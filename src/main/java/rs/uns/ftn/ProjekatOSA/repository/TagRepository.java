package rs.uns.ftn.ProjekatOSA.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.uns.ftn.ProjekatOSA.entity.Tag;
import rs.uns.ftn.ProjekatOSA.entity.User;

public interface TagRepository extends JpaRepository<Tag, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	
	List<Tag> findByUser (User user);
	
	Tag findByNameAndUser (String name, User user);
}
