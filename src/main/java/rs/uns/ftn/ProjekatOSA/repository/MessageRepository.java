package rs.uns.ftn.ProjekatOSA.repository;


import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import rs.uns.ftn.ProjekatOSA.entity.Message;

public interface MessageRepository extends JpaRepository<Message, Integer>{
	//samo rad sa entitetima tj. CRUD operacije (CREATE, READ, UPDATE, DELETE) i neke FIND operacije ako je potrebno
	
	List<Message> findBySubject (String subject);

	Optional<Message> findByDateTime(Date mailDate);

	Optional<Message> findById(Integer messageId);
}
