package rs.uns.ftn.ProjekatOSA.dto;

import java.io.Serializable;

import rs.uns.ftn.ProjekatOSA.entity.Contact;

public class ContactDTO implements Serializable {
	
	private int id;
	private String firstName;
	private String lastName;
	private String display;
	private String email;
	private PhotoDTO photo;
	private String note;
	
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	public ContactDTO(int id, String firstName, String lastName, String display, String email,
			PhotoDTO photo, String note) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.display = display;
		this.email = email;
		this.photo = photo;
		this.note = note;
	}

	public ContactDTO(Contact contact) {
		this.setId(contact.getId());
		this.setFirstName(contact.getFirstname());
		this.setLastName(contact.getLastname());
		this.setDisplay(contact.getDisplayName());
		this.setEmail(contact.getEmail());
		this.setNote(contact.getNote());
		
		if(contact.getPhoto() != null && contact.getPhoto().iterator().hasNext()) {
			this.setPhoto(new PhotoDTO(contact.getPhoto().iterator().next()));
		}
		else {
			this.setPhoto(new PhotoDTO());
		}
	}

	public ContactDTO() {
		super();
	}


	public int getId() {
		return id;
	}


	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public void setId(int id) {
		this.id = id;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getDisplay() {
		return display;
	}


	public void setDisplay(String display) {
		this.display = display;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}

	public PhotoDTO getPhoto() {
		return photo;
	}


	public void setPhoto(PhotoDTO photo) {
		this.photo = photo;
	}
	
	

}
