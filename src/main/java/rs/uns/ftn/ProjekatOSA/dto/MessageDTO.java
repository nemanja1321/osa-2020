package rs.uns.ftn.ProjekatOSA.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import rs.uns.ftn.ProjekatOSA.entity.Attachment;
import rs.uns.ftn.ProjekatOSA.entity.Message;
import rs.uns.ftn.ProjekatOSA.entity.Tag;

public class MessageDTO  implements Serializable{
	
	private int id;
	private String from;
	private String to;
	private List<String> cc;
	private List<String> bcc;
	private Date dateTime;
	private String subject;
	private String contetn;
	private List<AttachmentDTO> attachemnt;
	private List<TagDTO> tag;
	private boolean read;
	
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	public MessageDTO(int id, String from, String to, List<String> cc, List<String> bcc, Date dateTime,
			String subject, String contetn, List<AttachmentDTO> attachemnt, List<TagDTO> tag, boolean read) {
		super();
		this.id = id;
		this.from = from;
		this.to = to;
		this.cc = cc;
		this.bcc = bcc;
		this.dateTime = dateTime;
		this.subject = subject;
		this.contetn = contetn;
		this.attachemnt = attachemnt;
		this.tag = tag;
		this.read = read;
	}
	
	public MessageDTO(Message message) {
		List<AttachmentDTO> attachmentDTOs = new ArrayList<AttachmentDTO>();
		for(Attachment attachment : message.getAttachment()) {
			attachmentDTOs.add(new AttachmentDTO(attachment));
		}
		List<TagDTO> tagDTOs = new ArrayList<TagDTO>();
		if(message.getTag() != null) {
			for(Tag tag : message.getTag()) {
				tagDTOs.add(new TagDTO(tag));
			}
		}
		if(message.getId() != null) {
			this.setId(message.getId());
		}
		this.setFrom(message.getFrom());
		this.setTo(message.getTo());
		if(message.getTo() != null && !message.getTo().equals("")) {
			StringTokenizer token = new StringTokenizer(message.getCc(), ";");
			while (token.hasMoreTokens()) {
				this.getCc().add(token.nextToken());
			}
		}
		this.setBcc(new ArrayList());
		if(message.getBcc() != null && !message.getBcc().equals("")) {
			StringTokenizer token  = new StringTokenizer(message.getBcc(), ";");
			while(token.hasMoreTokens()) {
				this.getBcc().add(token.nextToken());
			}
		}
		this.setDateTime(message.getDateTime());
		this.setSubject(message.getSubject());
		this.setContetn(message.getContent());
		this.setTag(tagDTOs);
		this.setAttachemnt(attachmentDTOs);
		this.setRead(message.isUnread());
	}


	public MessageDTO() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getFrom() {
		return from;
	}


	public void setFrom(String from) {
		this.from = from;
	}


	public String getTo() {
		return to;
	}


	public void setTo(String to) {
		this.to = to;
	}


	public List<String> getCc() {
		return cc;
	}


	public void setCc(List<String> cc) {
		this.cc = cc;
	}


	public List<String> getBcc() {
		return bcc;
	}


	public void setBcc(List<String> bcc) {
		this.bcc = bcc;
	}


	public Date getDateTime() {
		return dateTime;
	}


	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getContetn() {
		return contetn;
	}


	public void setContetn(String contetn) {
		this.contetn = contetn;
	}


	public List<AttachmentDTO> getAttachemnt() {
		return attachemnt;
	}


	public void setAttachemnt(List<AttachmentDTO> attachemnt) {
		this.attachemnt = attachemnt;
	}


	public List<TagDTO> getTag() {
		return tag;
	}


	public void setTag(List<TagDTO> tag) {
		this.tag = tag;
	}


	public boolean isRead() {
		return read;
	}


	public void setRead(boolean read) {
		this.read = read;
	}
	
	

}
