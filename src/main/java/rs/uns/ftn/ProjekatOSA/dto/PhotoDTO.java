package rs.uns.ftn.ProjekatOSA.dto;

import java.io.Serializable;

import rs.uns.ftn.ProjekatOSA.entity.Photo;

public class PhotoDTO implements Serializable {
	
	private int id;
	private String path;
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	public PhotoDTO(int id, String path) {
		super();
		this.id = id;
		this.path = path;
	}

	public PhotoDTO() {
		super();
	}
	public PhotoDTO(Photo photo) {
		this.setId(photo.getId());
		this.setPath(photo.getPath());
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
	
	

}
