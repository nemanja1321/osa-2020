package rs.uns.ftn.ProjekatOSA.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.entity.Message;
import rs.uns.ftn.ProjekatOSA.entity.User;




public class AccountDTO implements Serializable {
	
	private Integer id;
	private String smtp;
	private String pop3Imap;
	private String username;
	private String password;
	private Integer smtpPort;
	private Integer inServerType;
	private String inServerAddress;
	private Integer inServerPort;
	private String Display;
	private List<MessageDTO> message;
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	
	
	
	
	public AccountDTO(Account account) {
		User user = account.getUser();
		List<MessageDTO> messageDTOs = new ArrayList<MessageDTO>();
		if(account.getMessage() != null) {
			for(Message itMessage : account.getMessage()) {
				messageDTOs.add(new MessageDTO(itMessage));
			}
		}
		this.setId(user.getId());
		this.setMessage(messageDTOs);
		this.setPassword(account.getPassword());
		this.setPop3Imap(account.getPop3Imap());
		this.setUsername(account.getUserName());
		this.setSmtp(account.getSmtpAdress());
		this.setInServerType(account.getInServerType());
		this.setDisplay(account.getDisplayName());
		this.setInServerPort(account.getInServerPort());
		this.setInServerAddress(account.getInServerAdress());
		this.setSmtpPort(account.getSmtpPort());
		
	
				
	}

	public AccountDTO(int id, String smtp, String pop3Imap, String username, String password, Integer smtpPort,
			Integer inServerType, String inServerAddress, Integer inServerPort, String display,
			List<MessageDTO> message) {
		super();
		this.id = id;
		this.smtp = smtp;
		this.pop3Imap = pop3Imap;
		this.username = username;
		this.password = password;
		this.smtpPort = smtpPort;
		this.inServerType = inServerType;
		this.inServerAddress = inServerAddress;
		this.inServerPort = inServerPort;
		Display = display;
		this.message = message;
	}
	
	

	public Integer getSmtpPort() {
		return smtpPort;
	}

	public void setSmtpPort(Integer smtpPort) {
		this.smtpPort = smtpPort;
	}

	public Integer getInServerType() {
		return inServerType;
	}

	public void setInServerType(Integer inServerType) {
		this.inServerType = inServerType;
	}

	public String getInServerAddress() {
		return inServerAddress;
	}

	public void setInServerAddress(String inServerAddress) {
		this.inServerAddress = inServerAddress;
	}

	public Integer getInServerPort() {
		return inServerPort;
	}

	public void setInServerPort(Integer inServerPort) {
		this.inServerPort = inServerPort;
	}

	public String getDisplay() {
		return Display;
	}

	public void setDisplay(String display) {
		Display = display;
	}

	public AccountDTO() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSmtp() {
		return smtp;
	}

	public void setSmtp(String smtp) {
		this.smtp = smtp;
	}

	public String getPop3Imap() {
		return pop3Imap;
	}

	public void setPop3Imap(String pop3Imap) {
		this.pop3Imap = pop3Imap;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<MessageDTO> getMessage() {
		return message;
	}

	public void setMessage(List<MessageDTO> message) {
		this.message = message;
	}
	
	

}
