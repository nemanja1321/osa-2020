package rs.uns.ftn.ProjekatOSA.dto;

import java.io.Serializable;

import org.aspectj.weaver.bcel.AtAjAttributes;

import rs.uns.ftn.ProjekatOSA.entity.Attachment;

public class AttachmentDTO implements Serializable {
	
	private int id;
	private String data;
	private String type;
	private String name;
	
	//klase koje nam spring vraca nakon obrade zahteva posaltog iz androida,
		//sadrze one atribute koji nam sadrze iste klase u Android app
	public AttachmentDTO(int id, String data, String type, String name) {
		super();
		this.id = id;
		this.data = data;
		this.type = type;
		this.name = name;
	}
	
	public AttachmentDTO(Attachment attachment) {
		if(attachment.getId() != null) {
			this.id = attachment.getId();
		}
		this.data = attachment.getData();
		this.name = attachment.getName();
		this.type = attachment.getMimeType();
	}


	public AttachmentDTO() {
		super();
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getData() {
		return data;
	}


	public void setData(String data) {
		this.data = data;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	

}
