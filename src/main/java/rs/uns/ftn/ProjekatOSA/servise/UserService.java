package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.uns.ftn.ProjekatOSA.entity.User;
import rs.uns.ftn.ProjekatOSA.repository.UserRepository;
@Service
public class UserService implements UserServiceInterface{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private UserRepository userRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	public Optional<User> findOne(Integer userId) {
		return userRepository.findById(userId);
	}

	public User save(User user) {
		return userRepository.save(user);
	}

	public List<User> findAll() {
		return userRepository.findAll();
	}

	public User findByUsername(String username) {
		return userRepository.findByUserName(username);
	}

	public User findByToken(String token) {
		return userRepository.findByToken(token);
	}

	public User findByUsernameAndPassword(String username, String password) {
		return userRepository.findByUserNameAndPassword(username, password);
	}

}
