package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import rs.uns.ftn.ProjekatOSA.entity.Account;

public interface AccountServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Account save (Account account);
	
	Optional<Account> findOne(Integer accountId);
	
	List<Account> findAll ();
	
	Account findByUsername (String username);
	
	Account findByToken (String token);
	
	Account findByUsernameAndPassword (String username, String password);
	
	void remove (Integer id);

	List<Account> findByUserId(Integer id);

}
