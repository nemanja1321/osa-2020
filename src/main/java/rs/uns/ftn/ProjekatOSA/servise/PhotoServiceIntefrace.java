package rs.uns.ftn.ProjekatOSA.servise;

import java.util.Optional;

import rs.uns.ftn.ProjekatOSA.entity.Photo;

public interface PhotoServiceIntefrace {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Photo save (Photo photo);
	
	Optional<Photo> findById (int id);
	
	void remove (Integer id);

}
