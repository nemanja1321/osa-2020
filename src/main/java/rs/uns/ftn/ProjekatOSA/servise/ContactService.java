package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.uns.ftn.ProjekatOSA.entity.Contact;
import rs.uns.ftn.ProjekatOSA.repository.ContactRepository;
@Service
public class ContactService implements ContactServiceInterface {

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private ContactRepository contactRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	public Optional<Contact> findOne(Integer contactId) {
		return contactRepository.findById(contactId);
	}

	public List<Contact> findAll() {
		return contactRepository.findAll();
	}

	public Contact save(Contact contact) {
		return contactRepository.save(contact);
	}

	public void remove(Integer id) {
		contactRepository.deleteById(id);
		
	}

	public List<Contact> findByUserId(Integer id) {
		return contactRepository.findByUserId(id);
	}

	

}
