package rs.uns.ftn.ProjekatOSA.servise;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.uns.ftn.ProjekatOSA.entity.Message;
import rs.uns.ftn.ProjekatOSA.repository.MessageRepository;
@Service
public class MessageSerivce implements MessageServiceInteface{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private MessageRepository messageRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	public Optional<Message> findOne(Integer messageId) {
		return messageRepository.findById(messageId);
	}

	public Message save(Message message) {
		return messageRepository.save(message);
	}

	public void remove(Integer id) {
		messageRepository.deleteById(id);
		
	}

	public List<Message> findAll() {
		return messageRepository.findAll();
	}

	public List<Message> findBySubject(String subject) {
		return messageRepository.findBySubject(subject);
	}

	public Optional<Message> findByDate(Date mailDate) {
		return messageRepository.findByDateTime(mailDate);
	}

}
