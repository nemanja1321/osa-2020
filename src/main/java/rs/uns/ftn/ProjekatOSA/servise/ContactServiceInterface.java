package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import rs.uns.ftn.ProjekatOSA.entity.Contact;

public interface ContactServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Optional<Contact> findOne (Integer contactId);
	
	List<Contact> findAll();
	
	Contact save (Contact contact);
	
	void remove (Integer id);

	List<Contact> findByUserId(Integer id);

}
