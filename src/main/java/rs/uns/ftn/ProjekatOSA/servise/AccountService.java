package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.repository.AccountRepository;
@Service
public class AccountService implements AccountServiceInterface {

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private AccountRepository accountRepository;

	public Account save(Account account) {
		return accountRepository.saveAndFlush(account);
	}

	public Optional<Account> findOne(Integer accountId) {
		return accountRepository.findById(accountId);
	}

	public List<Account> findAll() {
		return accountRepository.findAll();
	}

	public Account findByUsername(String username) {
		return accountRepository.findByUserName(username);
	}

	public Account findByToken(String token) {
		return accountRepository.findByToken(token);
	}

	public Account findByUsernameAndPassword(String username, String password) {
		return accountRepository.findByUserNameAndPassword(username, password);
	}

	public void remove(Integer id) {
		accountRepository.deleteById(id);
		
	}

	public List<Account> findByUserId(Integer id) {
		return accountRepository.findByUserId(id);
	}

	
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	
	
	

}
