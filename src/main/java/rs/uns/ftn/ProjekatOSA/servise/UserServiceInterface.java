package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import rs.uns.ftn.ProjekatOSA.entity.User;

public interface UserServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Optional<User> findOne (Integer userId);
	
	User save (User user);
	
	List<User> findAll();
	
	User findByUsername (String username);
	
	User findByToken (String token);
	
	User findByUsernameAndPassword (String username, String password);

}
