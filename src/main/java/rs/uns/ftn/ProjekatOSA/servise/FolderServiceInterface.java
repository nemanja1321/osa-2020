package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.entity.Folder;

public interface FolderServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	
	
	List<Folder> findAll();
	
	Folder save (Folder folder);
	
	void delete (Integer id);
	
	Folder findByName (String name);
	
	Folder findByNameAndAccount(String name, Account account);
	
	List<Folder> findByParent (Folder folder);

	void remove(Integer id);

	Optional<Folder> findOne(Integer id);

}
