package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.uns.ftn.ProjekatOSA.entity.Tag;
import rs.uns.ftn.ProjekatOSA.entity.User;
import rs.uns.ftn.ProjekatOSA.repository.TagRepository;
@Service
public class TagService implements TagServiceInterface{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private TagRepository tagRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	public Optional<Tag> findOne(Integer tagId) {
		return tagRepository.findById(tagId);
	}

	public Tag save(Tag tag) {
		return tagRepository.save(tag);
	}

	public void remove(Integer id) {
		tagRepository.deleteById(id);
		
	}

	public List<Tag> findAll() {
		return tagRepository.findAll();
	}

	public Tag findByNameAndUser(String name, User user) {
		return tagRepository.findByNameAndUser(name, user);
	}
}
