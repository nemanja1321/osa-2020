package rs.uns.ftn.ProjekatOSA.servise;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import rs.uns.ftn.ProjekatOSA.entity.Message;

public interface MessageServiceInteface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Optional<Message> findOne (Integer messageId);
	
	Message save (Message message);
	
	void remove (Integer id);
	
	List<Message> findAll();
	
	List<Message> findBySubject (String subject);

	Optional<Message> findByDate(Date mailDate);

}
