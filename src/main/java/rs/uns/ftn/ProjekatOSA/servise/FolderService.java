package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.uns.ftn.ProjekatOSA.entity.Account;
import rs.uns.ftn.ProjekatOSA.entity.Folder;
import rs.uns.ftn.ProjekatOSA.repository.FolderRepository;
@Service
public class FolderService implements FolderServiceInterface{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private FolderRepository folderRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	

	public List<Folder> findAll() {
		return folderRepository.findAll();
	}

	public Folder save(Folder folder) {
		return folderRepository.save(folder);
	}

	public void delete(Integer id) {
		folderRepository.deleteById(id);
		
	}

	public Folder findByName(String name) {
		return folderRepository.findByName(name);
	}

	public Folder findByNameAndAccount(String name, Account account) {
		return folderRepository.findByNameAndAccount(name, account);
	}

	public List<Folder> findByParent(Folder folder) {
		return folderRepository.findByParent(folder);
	}

	public void remove(Integer id) {
		folderRepository.deleteById(id);
		
	}

	public Optional<Folder> findOne(Integer folderId) {
		return folderRepository.findById(folderId);
	}

}
