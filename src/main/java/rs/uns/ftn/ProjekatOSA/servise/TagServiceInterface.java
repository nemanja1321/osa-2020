package rs.uns.ftn.ProjekatOSA.servise;

import java.util.List;
import java.util.Optional;

import rs.uns.ftn.ProjekatOSA.entity.Tag;
import rs.uns.ftn.ProjekatOSA.entity.User;

public interface TagServiceInterface {
	
	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	
	Optional<Tag> findOne (Integer tagId);
	
	Tag save (Tag tag);
	
	void remove (Integer id);
	
	List<Tag> findAll();
	
	Tag findByNameAndUser (String name, User user);

	

}
