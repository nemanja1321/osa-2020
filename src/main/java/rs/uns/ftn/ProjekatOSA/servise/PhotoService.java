package rs.uns.ftn.ProjekatOSA.servise;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.uns.ftn.ProjekatOSA.entity.Photo;
import rs.uns.ftn.ProjekatOSA.repository.PhotoRepository;
@Service
public class PhotoService implements PhotoServiceIntefrace{

	//NAS INTERFEJS SA METODAMA POTREBNIM ZA NASU APP
	@Autowired
	private PhotoRepository photoRepository;
	
	//STO BI REKO MILORAD SERVIS NE RADI NISTA PAMETNO SAMO POZIVA METODE REPOZITORIJUMA 
	
	public Photo save(Photo photo) {
		return photoRepository.save(photo);
	}

	public Optional<Photo> findById(int id) {
		return photoRepository.findById(id);
	}

	public void remove(Integer id) {
		photoRepository.deleteById(id);
		
	}

}
