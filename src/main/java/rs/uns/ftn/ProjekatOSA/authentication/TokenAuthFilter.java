package rs.uns.ftn.ProjekatOSA.authentication;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.web.filter.OncePerRequestFilter;

public class TokenAuthFilter extends OncePerRequestFilter {
	
	private final Log logger = LogFactory.getLog(this.getClass());
	
	private TokenHelper tokenHelper;
	
	private UserDetailsService userDetailsService;
	
	

	public TokenAuthFilter(TokenHelper tokenHelper, UserDetailsService userDetailsService) {
		this.tokenHelper = tokenHelper;
		this.userDetailsService = userDetailsService;
	}



	@Override
	public void doFilterInternal(HttpServletRequest request, HttpServletResponse resposne, FilterChain chain)
			throws ServletException, IOException {
		String username;
		String authToken = tokenHelper.getToken(request);
		
		if(authToken != null) {
			username = tokenHelper.getUsernameFromToken(authToken);
			if(username != null) {
				UserDetails userDetails = userDetailsService.loadUserByUsername(username);
				if(tokenHelper.validateToken(authToken, userDetails)) {
					TokenBasedAuthentication authentication = new TokenBasedAuthentication(userDetails);
					authentication.setToken(authToken);
					SecurityContextHolder.getContext().setAuthentication(authentication);
				}
			}
		}
		chain.doFilter(request, resposne);
	}

}
