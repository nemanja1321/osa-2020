package rs.uns.ftn.ProjekatOSA.authentication;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Component
public class TokenHelper {
	
	static final String AUDIENCE_WEB = "web";
	
	private static final SignatureAlgorithm SIGNATURE_ALGORITHM = SignatureAlgorithm.HS512;
	
	@Value("somesecret")
	public String SECRET;
	
	@Value("SpringProject")
	private String APP_NAME;
	
	@Value("300")
	private int EXPIRES_IN;
	
	@Value("Authorization")
	private String AUTH_HEADER;
	
	public String getUsernameFromToken(String token) {
		String username;
		try {
			final Claims claims = this.getAllClaimsFromToken(token);
			username = claims.getSubject();
		}
		catch (Exception e) {
			username = null;
		}
		return username;
	}
	
	public Date getIssuedAtDateFromToken(String token) {
		Date issueAt;
		try {
			final Claims claims = this.getAllClaimsFromToken(token);
			issueAt = claims.getIssuedAt();
		}
		catch (Exception e) {
			 issueAt = null;
		}
		return issueAt;
	}
	
	public String getAudienceFromToken(String token) {
		String audience;
		try {
			final Claims claims = this.getAllClaimsFromToken(token);
			audience = claims.getAudience();
		}
		catch (Exception e) {
			audience = null;
		}
		return audience;
	}
	
	public String refreshToken(String token) {
		String refreshedToken;
		Date a = new Date();
		try {
			final Claims claims = this.getAllClaimsFromToken(token);
			claims.setIssuedAt(a);
			refreshedToken = Jwts.builder().setClaims(claims).setExpiration(generateExparationDate()).signWith(SIGNATURE_ALGORITHM, SECRET).compact();
		}
		catch (Exception e) {
			refreshedToken = null;
		}
		return refreshedToken;
	}
	
	public String generateToken(String username) {
		return Jwts.builder().setIssuer(APP_NAME).setSubject(username).setIssuedAt(new Date()).signWith(SIGNATURE_ALGORITHM, SECRET).compact();
	}
	
	public int getExpiredIn() {
		return EXPIRES_IN;
	}
	
	public Boolean validateToken(String token, UserDetails userDetails) {
		UserDetails user = userDetails;
		final String username = getUsernameFromToken(token);
		final Date created = getIssuedAtDateFromToken(token);
		return (username != null && username.equals(userDetails.getUsername()));
	}
	
	private Boolean isCreatedBeforeLastPasswordReset(Date created, Date lastPasswordReset) {
		return (lastPasswordReset != null && created.before(lastPasswordReset));
	}
	
	public String getToken(HttpServletRequest request) {
		String header = getAuthHeaderFromHeader(request);
		if (header != null && header.startsWith("Bearer ")) {
			return header.substring(7);
		}
		return null;
	}
	
	public String getAuthHeaderFromHeader(HttpServletRequest request) {
		return request.getHeader(AUTH_HEADER);
	}
	private Claims getAllClaimsFromToken(String token) {
		Claims claims;
		try {
			claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
		}
		catch (Exception e) {
			claims = null;
		}
		return claims;
	}
	
	private Date generateExparationDate() {
		long expiresIn = EXPIRES_IN;
		return new Date(new Date().getTime() + expiresIn * 1000);
	}

}
