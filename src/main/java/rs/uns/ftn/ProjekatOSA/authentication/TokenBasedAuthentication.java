package rs.uns.ftn.ProjekatOSA.authentication;

import java.util.Collection;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

public class TokenBasedAuthentication extends AbstractAuthenticationToken {
	
	private String token;
	private final UserDetails principale;

	public TokenBasedAuthentication(UserDetails userDetails) {
		super(userDetails.getAuthorities());
		this.principale = userDetails;
		
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Object getCredentials() {
		
		return token;
	}

	public UserDetails getPrincipal() {
		
		return principale;
	}
	
	public boolean isAutheticated() {
		return true;
	}
	
}
